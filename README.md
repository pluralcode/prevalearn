# README #

This is the repository we'll use for development of the PrevaLearn project.

### Dependencies
#### OSX: (use homebrew)

```shell
brew install postgis geos imagemagick
```

#### Windows:
 No idea...

#### Linux:
  Distribution dependent   
  Ubuntu:
  ```shell
sudo apt-get install postgis libgeos-3.4.2 libgeos-c1 libgeos-dev imagemagick -y
  ```


## Installation

Assuming Ruby and PostgreSQL are already installed...   

```shell
git clone <repo>   
cd <project_dir>   
bundle install   
rails g geokit_rails:install
rake db:create   
rake db:migrate   
rake db:seed  
```

If you already have a db setup for this project you can do   
```shell
rake db:gis:setup
```

though it is recommended to drop the db and start fresh   
```shell
rake db:drop
```


You MUST have Postgresql >= 9.1

Be sure your adapter is set to 'postgis' in config/database.yml BEFORE rake create/migrate

```
adapter: postgis
```




### Infos
https://github.com/rgeo/activerecord-postgis-adapter    
https://github.com/rgeo/rgeo   
http://postgis.net/install/   
https://github.com/geokit/geokit-rails   


### Notes
If git is still tracking .log files and anything in tmp/ or subdirectories of public/ try issuing these commands in the project dir

```git  
  git rm --cached development.log
  git rm -r --cached tmp/
  git rm -r --cached public/assets/
```

Also, note that we DO want the html, favicon and robots.txt files in public to be tracked.
