require 'test_helper'

class StaticPagesControllerTest < ActionController::TestCase
  test "should get teach_with_us" do
    get :teach_with_us
    assert_response :success
  end

  test "should get how_it_works" do
    get :how_it_works
    assert_response :success
  end

end
