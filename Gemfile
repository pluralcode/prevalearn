source 'https://rubygems.org'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '4.2.5.2'

# Use postgresql as the database for Active Record
gem 'pg', '~> 0.15'

# Use postgis with postgresql for geographical information systems
gem 'activerecord-postgis-adapter'

# Adds the ability to use IP for location services
gem 'geokit-rails'

# Rails console output prettifier
gem 'hirb', '~>0.7.3'

# Use SCSS for stylesheets
gem 'bootstrap-sass', '~> 3.3.6'
gem 'sass-rails', '~> 5.0'

# Use font-awesome icons
gem "font-awesome-rails"

# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'

# Use CoffeeScript for .coffee assets and views
gem 'coffee-rails', '~> 4.1.0'

# See https://github.com/rails/execjs#readme for more supported runtimes
gem 'therubyracer', platforms: :ruby

# Use jquery as the JavaScript library
gem 'jquery-rails'

# Use jquery-ui to enhance user experience
gem 'jquery-ui-rails'

# Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
gem 'turbolinks'

# Fix problems brought about turbolinks
gem 'jquery-turbolinks'

# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.0'

# bundle exec rake doc:rails generates the API under doc/api.
gem 'sdoc', '~> 0.4.0', group: :doc

# Use simple form
gem 'simple_form'

# Rails date picker
gem 'momentjs-rails', '~> 2.9',  :github => 'derekprior/momentjs-rails'
gem 'datetimepicker-rails', github: 'zpaulovics/datetimepicker-rails', branch: 'master', submodules: true

# Use wicked to create form wizards
gem 'wicked'

# Use haml
gem "haml-rails", "~> 0.9"

# Use figaro to manage ENV variables
gem "figaro"

# Store user supplied assets in the cloud
gem 'aws-sdk', '~> 2.3'

# Use devise as an Authentication solution
gem 'devise', "~> 3.5.6"

# Useful information about countries
gem 'countries'

# Validate url
gem "validate_url"

group :development, :test do
  # Call 'pry' anywhere in the code to stop execution and get a debugger console
  gem 'pry'
end

gem 'time_ago_in_words'

group :development do
	# Preview emails in browser
	gem "letter_opener"

	# Access an IRB console on exception pages or by using <%= console %> in views
	gem 'web-console', '~> 2.0'

	# Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
	gem 'spring'
end

# Use Puma web server
gem 'puma'

# Production enjoy all platform features
gem 'rails_12factor', group: :production

# HTML Email Styling
gem 'roadie', '~> 3.1.1'

# Use  paperclip to upload and process images
gem "paperclip", "~> 5.0.0.beta1"

# Use ActiveModel has_secure_password
gem 'bcrypt', '~> 3.1.7'

# Admin panel
gem "administrate", "~> 0.2.0"
gem "administrate-field-image"

ruby "2.2.3"
