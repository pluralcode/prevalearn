Rails.application.routes.draw do


    namespace :admin do
        resources :users
        resources :age_requirements
        resources :app_countries
        resources :awards
        resources :categories
        resources :class_levels
        resources :disciplines
        resources :genders
        resources :individuals
        resources :industries
        resources :lessons
        resources :lesson_galleries
        resources :lesson_schedules
        resources :lesson_specifications
        resources :objectives
        resources :organizations
        resources :profiles
        resources :qualifications
        resources :schools_attendeds
        resources :sub_categories
        resources :sub_sub_categories
        resources :tutor_qualifications
        resources :user_types

        root to: "users#index"
    end

    get 'dashboard', to: 'dashboard#index', as: :dashboard
    
    get '/shortlisted_lessons' => 'shortlisted_lessons#index'
    post '/lessons/:id/shortlist' => 'shortlisted_lessons#create', as: :shortlist_lesson
    post '/shortlisted_lessons/delete' => 'shortlisted_lessons#delete', as: :delete_shortlisted_lessons

    get '/inbox' => 'chats#index', as: :chats
    get '/chats/:id' => 'chats#show', as: :chat
    get 'lessons/:id/chats' => 'chats#create', as: :create_chat
    post 'chats/perform-action' => 'chats#perform_action_on_chats', as: :perform_action_on_chats
    get '/chats/:id/star' => 'chats#toggle_chat_star', as: :toggle_chat_star

    post '/chats/:chat_id/messages' => 'messages#create', as: :message
    get 'chats/:id/update-chat-box' => 'messages#update_chat_box', as: :update_chat_box

    post 'lessons/:id/book-with-chat' => 'bookings#create_with_chat', as: :booking_with_chat
    post 'lessons/:id/book-without-chat' => 'bookings#create_without_chat', as: :booking_without_chat
    patch '/bookings/:id/accept' => 'bookings#accept', as: :accept_booking
    get '/bookings/manage' => 'bookings#manage', as: :manage_bookings
    put 'lesson_schedule/:id/bookings/perform-action' => 'bookings#perform_action', as: :perform_action_on_bookings
    delete '/bookings/:id/cancel' => 'bookings#cancel', as: :cancel_booking
    get '/bookings/student/:id/profile' => 'bookings#student_profile', as: :student_profile
    get '/my-lessons/:id/bookings' => 'bookings#lesson_bookings', as: :lesson_bookings
    get '/enrolled-lessons/:id/bookings' => 'bookings#enrolled_lesson_bookings', as: :enrolled_lesson_bookings
    get '/bookings/:id/confirm-payment' => 'bookings#confirm_payment', as: :confirm_payment
    get '/bookings/:id/send-payment-instructions' => 'bookings#send_payment_instructions', as: :send_payment_instructions 
    
    get '/reviews' => 'reviews#index', as: :reviews
    post '/reviews' => 'reviews#create', as: :create_review
    
    post '/reported_users' => 'reported_users#create', as: :reported_user
    
    resources :accounts do
        collection do
            get :verify_account
            get :delete_account
            get :problem_report
            put :confirm_verification
            post :confirm_delete
            post :email_report
        end
    end

    resources :lessons do
        member do
            get :basic_info
            get :lesson_info
            get :work_and_education
            get :awards
            get :rates
            get :other_settings
        end
    end

    resource :search, only: [:show], controller: :search

    resources :categories, only: [:show]
    resources :sub_categories, only: [:show]
    resources :awards, only: [:edit, :update, :destroy]
    resources :after_signup
    resources :profiles, only: [:update]
    resources :app_countries, only: :show
    resources :lesson_galleries, only: [:destroy]

    devise_for :users, controllers: { registrations: "registrations" }

    post '/payment_setting', to: 'accounts#payment'
    get 'select_change', to: 'select_change#index', as: :select_change
    get '/teach_with_us', to: 'static_pages#teach_with_us'
    get '/how_it_works', to: 'static_pages#how_it_works'
    get '/contact_us', to: 'static_pages#contact_us'
    get '/about_us', to: 'static_pages#about_us'
    get '/privacy', to: 'static_pages#privacy'
    get '/terms', to: 'static_pages#terms'
    get '/home', to: 'static_pages#home'
    get '/faq', to: 'static_pages#faq'

    root 'static_pages#home'


end
