class ChatsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_chat, only: [:show, :toggle_chat_star]


  def index
    chats = [Chat.sender_chats(current_user.id), Chat.recipient_chats(current_user.id)].flatten.uniq.sort{ |a, b| b.created_at <=> a.created_at }
    unread_messages = chats.select{|c| c.is_chat_has_unread_messages?}.flatten
    @all_chats = chats.unshift(unread_messages).flatten.uniq

    @ss_chats = @all_chats.select{|c| c.sender_id == current_user.id }
    @sp_chats = @all_chats.select{|c| c.recipient_id == current_user.id }

    @chats = if params[:cut].present?
      @all_chats.select{|c| c.send("#{params[:cut] == 'ss' ? 'sender_id' : 'recipient_id'}") == current_user.id }
    elsif params[:archived].present?
      @all_chats.select{|c| c.archived == params[:archived] }
    else
      @all_chats
    end
  end

  def show
    booking = @chat.booking 
    if booking.present? && booking.lesson_schedule.lesson_date < Date.today && !booking.review_left?
      booking.update_attribute(:status, 'completed')
    end
    unread_messages = @chat.messages.select{|m| !m.read? && m.recipient == current_user}
    
    if unread_messages.present?
      @chat.update_attribute(:deleted_by_sender, false)
      @chat.update_attribute(:deleted_by_recipient, false)
    end  
    
    unread_messages.map{|m| m.update_attribute(:read, true)} 
  end

  def create
    lesson = Lesson.find_by(id: params[:id])
    chat = Chat.where(lesson_id: lesson.id).last

    if chat.nil?
      chat = Chat.new({
        sender_id: current_user.id,
        recipient_id: lesson.user.id,
        lesson_id: lesson.id
      })

      chat.save
    end
    redirect_to chat_path(chat.id)
  end

  def perform_action_on_chats
    selected_chats = Chat.where(id: params["selected_chats"].values)
    selected_chats.each do |chat|  
      update_attrs = perform_action_update_attrs(params[:chats][:action_to_perform], chat)
      chat.update_attribute(update_attrs.first, update_attrs.last)
    end
    redirect_to request.referer, notice: 'Selected chats have been updated successfully.'
  end

  def toggle_chat_star
    @chat.update_attribute(chat_star_field, !chat_star_status)
    
    redirect_to request.referer, notice: "Chat successfully #{chat_star_status ? 'favourited' : 'unfavourited'}."
  end

  private
  def set_chat
  	@chat = Chat.find(params[:id])
  end

  def perform_action_update_attrs(action, chat)
    if chat.sender?(current_user)
      attrs = [:starred_by_sender, true] if action == 'star-chats'
      attrs = [:starred_by_sender, false] if action == 'unstar-chats'
      attrs = [:deleted_by_sender, true] if action == 'delete-chats'
      attrs
    else
      attrs = [:starred_by_recipient, true] if action == 'star-chats'
      attrs = [:starred_by_recipient, false] if action == 'unstar-chats'
      attrs = [:deleted_by_recipient, true] if action == 'delete-chats'
      attrs
    end
  end

  def chat_star_status
    @chat.send(chat_star_field)
  end

  def chat_star_field
    @chat.sender?(current_user) ? :starred_by_sender : :starred_by_recipient
  end
end
