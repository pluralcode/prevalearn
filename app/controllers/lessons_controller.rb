class LessonsController < ApplicationController
	before_action :authenticate_user!
	before_action :set_lesson, except: [:new, :index, :create]

	def index
		@lessons = current_user.lessons.order('created_at DESC')
	end

	def new
		@lesson = current_user.lessons.new
	end

	def show
		# @subcategories = SubCategory.where(category_id:params[:id])
		@lesson = Lesson.eager_load(user: :profile).eager_load([:lesson_galleries, :lesson_categories, :lesson_specification]).where(id: params[:id]).first

		@instructor = @lesson.user
	end

	def create
		@lesson = current_user.lessons.new(lesson_params)

		respond_to do |format|
			if @lesson.save
				format.html { redirect_to basic_info_lesson_url(@lesson), notice: 'lesson successfully created' }
				format.json { }
			else
				flash[:error] = "An error occured. Please make sure that you included the title"
				format.html { redirect_to lessons_url }
				format.json { }
			end
		end
	end

	def update
		# render json:params
		lat = params[:lesson][:lesson_specification_attributes].delete(:code_lat) rescue String.new
		lng = params[:lesson][:lesson_specification_attributes].delete(:code_lng) rescue String.new

		params[:lesson][:latlon] = "POINT(#{lng} #{lat})" if !lat.empty? && !lng.empty?

		# render json:lesson_params
		respond_to do |format|
			photo_attr = lesson_params[:lesson_galleries_attributes]['0'] unless lesson_params[:lesson_galleries_attributes].nil?
			if @lesson.update(lesson_params.except!(:lesson_galleries_attributes))
				if photo_attr && photo_attr['photo'].present?
					@lesson.lesson_galleries.create(photo_attr)
				end
				format.html { redirect_to request.referer, notice: 'information successfully updated' }
				format.json { }
			else
				flash[:error] = 'An error occured during update. Please try again'
				format.html { redirect_to request.referer }
				format.json { render json: @lesson.errors }
			end
		end
	end

	def basic_info
		if @lesson.lesson_galleries.empty?
			@lesson.lesson_galleries.build
		end
	end

	def lesson_info
		if @lesson.lesson_specification.blank?
			@lesson.build_lesson_specification
		end

		if @lesson.lesson_categories.empty?
			@lesson.lesson_categories.build
		end
	end

	def work_and_education
		if @lesson.schools_attendeds.empty?
			@lesson.schools_attendeds.build
		end
		if @lesson.tutor_qualification.blank?
			@lesson.build_tutor_qualification
		end
	end

	def awards
		@awards = @lesson.awards
	end

	def rates
		@schedules = @lesson.lesson_schedules.order("created_at asc")
	end

	def other_settings

	end

	def lesson_management
	end

	private
		def lesson_params
			params.require(:lesson).permit(
				:title, :about_tutor, :category_id, :sub_category_id, :is_age_public,
				:sub_sub_category_id, :class_description, :has_schedule, :is_hidden_from_listing,:latlon,
				lesson_galleries_attributes: [:photo, :lesson_id],
				lesson_schedules_attributes: [:id, :lesson_date, :from_time, :to_time, :lesson_id, :rate, :_destroy],
				lesson_categories_attributes: [:id, :lesson_id, :category_id, :sub_category_id, :sub_sub_category_id, :_destroy],
				lesson_specification_attributes: [:id, :class_level_id, :age_requirement_id, :average_class_size, :number_of_lessons,
					:fee_includes, :class_location, :lesson_id, :code_lat, :code_lng],
				awards_attributes: [:id, :lesson_id, :name, :year, :_destroy],
				schools_attendeds_attributes: [:id, :name, :start_year, :end_year, :has_graduated, :disciplines, :qualification_id],
				tutor_qualification_attributes: [:id, :years_of_experience])
		end

		def set_lesson
			@lesson = Lesson.find(params[:id])
		end
end
