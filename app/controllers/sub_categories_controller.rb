class SubCategoriesController < ApplicationController
  include Searchable
  before_action :set_sub_category, only: [:show]

  # GET /awards/1
  # GET /awards/1.json
  def show
    find_lessons
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_sub_category
      @sub_category = SubCategory.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    # def award_params
    #   params.require(:award).permit(:name, :year)
    # end
end
