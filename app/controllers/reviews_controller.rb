class ReviewsController < ApplicationController

  def create
    review = Review.new(review_params)
    if review.save
      
      booking = Booking.find_by(id: params[:booking_id])
      booking.update_attribute(:status, 'review_left')
      create_message(booking.student_id, booking.instructor_id, booking.chat.id, "#{booking.student.display_name} has left a review.")
      
      redirect_to request.referer, notice: 'Your review has been submitted successfully.'
    end
  end

  def index
    @reviews = Review.where(reviewee_id: current_user.id)
  end

  private
  def review_params
    params.require(:reviews).permit(:title, :content, :rating, :reviewer_id, :lesson_id, :reviewee_id)
  end

  def create_message(sender_id, recipient_id, chat_id, content)
    Message.create({
      content: content,
      sender_id: sender_id,
      recipient_id: recipient_id,
      chat_id: chat_id,
      read: false
    })
  end
end
