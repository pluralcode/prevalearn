class SearchController < ApplicationController
  include Searchable

  def show
    find_lessons
  end
end
