module Searchable
  def find_lessons
    set_em_up

    @inclusion = params[:controller] == 'search'
    @random = !!params[:limit]

    if @category
      @lessons = @lessons.where("lesson_categories.category_id = ?", @category.id)
      @subcategories = SubCategory.where(category_id: @category.id).order(:name)
    elsif @sub_category
      @lessons = @lessons.where("lesson_categories.sub_category_id = ?", @sub_category.id)
      @subcategories = SubCategory.where(category_id: @sub_category.category.id).order(:name)
    elsif @sub_sub_category
      @lessons = @lessons.where("lesson_categories.sub_sub_category_id = ?", params[:id])
    end

    knock_em_down

    respond_to do |format|
      format.html { render :show }
      format.json { render json: @lessons, include: [
        lesson_categories: { include: [:category] }
        ]}
    end
  end

  private
    def set_em_up
      @order = params[:search][:sort_by].intern rescue :last_login
      @range = params[:search][:range_from].scan(/^\d+(?:\.\d+)?/).first.to_d rescue nil
      @location = params[:search][:browse_from] rescue nil

      @rangeable = !@range.nil? && @range > 0

      set_location if @rangeable || @order == :nearest

      # @lessons = Lesson.joins(lesson_categories: {category: {sub_categories: :sub_sub_categories}}).
      @lessons = Lesson.joins("INNER JOIN lesson_categories ON lesson_categories.lesson_id = lessons.id
      INNER JOIN categories ON categories.id = lesson_categories.category_id
      LEFT OUTER JOIN sub_categories ON sub_categories.category_id = categories.id
      LEFT OUTER JOIN sub_sub_categories ON sub_sub_categories.sub_category_id = sub_categories.id" ).
                     includes(:lesson_galleries).
                     joins(:lesson_schedules).
                     joins(user: :profile).
                     joins(:lesson_specification)
    end

    def knock_em_down
      if params[:search]
        set_parameters
        filter_lessons
      end

      order_by

      @highest = params[:search][:rates_to] rescue nil
      @lowest  = params[:search][:rates_from] rescue nil

      @lessons = @lessons.limit(params[:limit]) if params[:limit]

      @lessons = @lessons.to_a.uniq
    end

    def user_country_latlon
      current_user.profile.app_country.latlon
    end

    def order_by
      @order = :last_login if @order == :nearest && (@latitude.nil? || @longitude.nil?)

      if @random
        previous = @order
        @order = :random
      end

      case @order
      when :random
        @lessons = @lessons.order('RANDOM()')
      when :nearest
        @lessons = @lessons.order("ST_DISTANCE_SPHERE(ST_GeomFromWKB(lessons.latlon, 4326), ST_GeomFromText('POINT(#{@longitude} #{@latitude})', 4326))")
      when :lowest_price
        @lessons = @lessons.order('lesson_schedules.rate')
      when :highest_price
        @lessons = @lessons.order('lesson_schedules.rate DESC')
      else
        @lessons = @lessons.order('users.last_sign_in_at')
      end

      @order = previous if @random
    end

    def set_location
      options = params[:search]

      # user supplied
      if (options[:code_lat] && !options[:code_lat].strip.empty?) && (options[:code_lng] && !options[:code_lng].strip.empty?)
        latitude  = options[:code_lat]
        longitude = options[:code_lng]
      end

      # user IP, specific
      begin
        location = Geokit::Geocoders::IpGeocoder.geocode(request.remote_ip)

        if location.success
          latitude  ||= location.lat
          longitude ||= location.lng
        end
      rescue => e
        puts e.inspect
      end

      # user IP, alternate
      begin
        latitude  ||= session[:geo_location]['lat'] if session[:geo_location]['lat']
        longitude ||= session[:geo_location]['lng'] if session[:geo_location]['lng']
      rescue => e
        puts e.inspect
      end

      # user country
      begin
        latlon = user_country_latlon

        longitude ||= latlon.longitude
        latitude  ||= latlon.latitude
      rescue => e
        puts e.inspect
      end

      # sanitize possible user input
      @latitude  = latitude.to_s.match(/^(-?\d+(\.\d+)?)/)[0]  rescue nil
      @longitude = longitude.to_s.match(/^(-?\d+(\.\d+)?)/)[0] rescue nil
    end

    def set_parameters
      @lowest  = [params[:search][:rates_from].to_d.round(2), 0].max rescue 0
      @highest = [params[:search][:rates_to].to_d.round(2), 0].max rescue 0
      @query   = params[:search][:search].strip rescue String.new

      @lowest, @highest = @highest, @lowest if @lowest > @highest && !@highest.zero?
    end

    def filter_lessons
      @lessons = @lessons.where("ST_DWithin(lessons.latlon, ST_SetSRID(ST_Point(:lon, :lat), 4326), :distance)", lon: @longitude, lat: @latitude, distance: @range * 1000) if @rangeable && (@longitude && @latitude)
      @lessons = @lessons.where("lesson_schedules.rate >= ?", @lowest) unless @lowest.zero?
      @lessons = @lessons.where("lesson_schedules.rate <= ?", @highest) unless @highest.zero?

      unless @query.empty?
        if @inclusion
          @lessons = @lessons.where("lessons.title ILIKE :term OR categories.name ILIKE :term OR sub_categories.name ILIKE :term OR sub_sub_categories.name ILIKE :term", term: "%#{@query}%")
        else
          @lessons = @lessons.where("lessons.title ILIKE ?", "%#{@query}%")
        end
      end
    end

    def search_params
      params.require(:search).permit(
        :rates_to, :rates_from, :code_lat, :code_lng,
        :browse_from, :range_from, :sort_by, :search,
        :nearest, :lowest_price, :highest_price, :last_login,
        :limit )
    end
end
