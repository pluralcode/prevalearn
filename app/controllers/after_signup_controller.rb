class AfterSignupController < ApplicationController
	include Wicked::Wizard
	skip_before_action :profile_exists?
	before_action :authenticate_user!

	steps :step_one, :step_two, :step_three, :step_four, :finish

	def show
		case step
		when :step_one
			@resource = current_user.build_profile
			@user_types = UserType.where(is_active: true)
		else
			@resource = current_user.profile.profilable
		end

		render_wizard
	end

	def update
		resource_name = params[:resource]
		begin
			case resource_name
			when "Profile"
				user_type = UserType.find(profile_params[:user_type_id])
				klass = user_type.name.constantize
				params = { profile_attributes: {
					user_id: current_user.id, user_type_id: user_type.id
				} }
				@resource = klass.new(params)
			when "Individual"
				@resource = current_user.profile.profilable
				@resource.attributes = individual_params
			when "Organization"
				@resource = current_user.profile.profilable
				@resource.attributes = organization_params
			end

			render_wizard @resource
		rescue StandardError => e
			logger.info e
			if resource_name.eql?("Profile")
				redirect_to wizard_path(:step_one)
			else
				render step
			end
		end
  	end

  	private
  		def profile_params
  			params.require(:profile).permit(
  				:user_type_id, :user_id, :app_country_id,
  				:profilable_id, :profilable_type
  			)
  		end

  		def individual_params
  			params.require(:individual).permit(
  				:postal_code, :first_name, :last_name, :gender_id,
  				:objective_id, :contact_number, :date_of_birth, :step,
  				profile_attributes: [:id, :user_id, :user_type_id,
  					:app_country_id]
  			)
  		end

  		def organization_params
  			params.require(:organization).permit(
  				:name, :address, :industry_id, :registration_number, :step,
  				:website, :contact_number, profile_attributes: [ :id,
  					:user_id, :user_type_id, :app_country_id]
  			)
  		end

  		def finish_wizard_path
  			dashboard_url
  		end
end
