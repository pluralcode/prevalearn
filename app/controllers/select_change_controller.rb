class SelectChangeController < ApplicationController
	def index
		case params[:klass]
		when "category"
			klass = params[:klass].capitalize.constantize.find(params[:object_id])
			@result = klass.sub_categories.order('name ASC')
		when "sub_category"
			klass = params[:klass].camelize.constantize.find(params[:object_id])
			@result = klass.sub_sub_categories.order('name ASC')
		end

		respond_to do |format|
			format.json { render json: @result, status: :ok }
		end
	end
end
