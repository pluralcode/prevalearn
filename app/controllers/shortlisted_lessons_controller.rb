class ShortlistedLessonsController < ApplicationController
  def index
  	@shortlisted_lessons = current_user.shortlisted_lessons
  end

  def create
  	shortlisted_lesson = ShortlistedLesson.find_by(params[:shortlisted_lesson])
  	
  	if shortlisted_lesson.nil?
	  	@shortlisted_lesson = ShortlistedLesson.create(shortlisted_lesson_params)
	  else
		  shortlisted_lesson.delete
	  end

  	redirect_to request.referrer
  end

  def delete
    shortlisted_lessons = ShortlistedLesson.where(id: params[:shortlisted_lessons].values).delete_all
    redirect_to request.referrer, notice: 'Selected shortlisted lessons deleted successfully.'
  end

  private
  def shortlisted_lesson_params
  	params.require(:shortlisted_lesson).permit(:user_id, :lesson_id)
  end
end
