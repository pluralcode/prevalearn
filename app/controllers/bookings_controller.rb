require 'csv'
class BookingsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_lesson_data, only: [:create_with_chat, :create_without_chat]
  before_action :set_booking, only: [:accept, :cancel, :confirm_payment, :send_payment_instructions]

  def create_with_chat
    build_booking
    if @booking.save
      build_chat
      if @chat.save
        send("create_#{@booking.status}_state_messages", @booking, @booking.chat)
        redirect_to request.referer, notice: 'Your booking has been requested successfully.'
      end
    end
  end

  def create_without_chat
    build_booking
    if @booking.save
      chat = Chat.find_by(id: params[:chat_id])
      chat.update_attribute(:booking_id, @booking.id)
      send("create_#{@booking.status}_state_messages", @booking, chat)
      redirect_to request.referer, notice: 'Your booking has been requested successfully.'
    end
  end

  def accept
    accept_status = Booking.status_due_to_payme(inst_pay_me(@booking))
    if @booking.update_attribute(:status, accept_status)
      send("create_#{@booking.status}_state_messages", @booking)
      respond_to do |format|
        format.js
      end
    end
  end

  def manage
    @my_lessons = Booking.having_statuses(['pending', 'confirmed', 'declined', 'accepted' , 'payment_instructions_sent', 'completed']).where(instructor_id: current_user.id).group_by{|b| b.lesson }
    @enrolled_lessons = Booking.having_statuses(['pending', 'confirmed', 'declined', 'accepted' , 'payment_instructions_sent', 'completed']).where(student_id: current_user.id).group_by{|b| b.lesson }
  end

  def lesson_bookings
    @model_to_open = cookies[:model_to_open]
    cookies[:model_to_open] = nil
    @bookings = Booking.having_statuses(['pending', 'confirmed', 'accepted', 'payment_instructions_sent', 'declined', 'completed']).where(lesson_id: params[:id], instructor_id: current_user.id).group_by{|b| b.lesson_schedule }
  end

  def enrolled_lesson_bookings
    @bookings = Booking.having_statuses(['pending', 'confirmed', 'accepted', 'payment_instructions_sent', 'declined', 'cancelled', 'completed']).where(lesson_id: params[:id], student_id: current_user.id).group_by{|b| b.lesson_schedule }
  end

  def perform_action
    
    selected_bookings = Booking.where(id: params["bookings"][params[:id]].values)
    action_to_perform = params["bookings"]["action_to_perform"]
    if action_to_perform != 'export_to_csv'
      
      if action_to_perform != "delete"
        selected_bookings.map{|b| b.update_attribute(:status, action_to_perform)}
        selected_bookings.map{|b| send("create_#{b.status}_state_messages", b) }
      else
        deletable_requests = selected_bookings.having_statuses(['pending', 'declined'])
        deletable_requests.each do |b|
          create_message(b.instructor_id, b.student_id, b.chat.id, "#{b.instructor.display_name} has rejected your booking request.")
        end

        deletable_requests.delete_all
      end
      cookies[:model_to_open] = params[:bookings][:model_to_open]
      redirect_to request.referer, notice: flash_messages_on_perform_action(action_to_perform)
    else
      csv_file = Tempfile.new(['Lesson-requests', '.csv'])
      CSV.open(csv_file, "wb") do |csv|
        csv << ['Full Name', 'Gender', 'Email', 'Mobile', 'Date of Birth', 'Address', 'Zipcode', 'Paid']

        selected_bookings.each do |booking|
          csv << [
            booking.student.display_name,
            booking.student.gender,
            booking.student.email,
            booking.student.profile.try(:profilable).try(:contact_number),
            booking.student.date_of_birth,
            booking.student.profile.try(:profilable).try(:address),
            '',
            booking.confirmed? ? 'Yes' : 'No'
          ]
        end
      end  
      send_file csv_file.path
    end
  end

  def confirm_payment
    if @booking.update_attribute(:status, 'confirmed')
      create_confirmed_state_messages(@booking)
      redirect_to request.referer, notice: 'Payment for the booking has been confirmed successfully'
    end
  end

  def cancel
    chat = @booking.chat
    chat.update_attribute(:booking_id, nil)
    create_cancel_state_messages(@booking)

    @booking.delete
    redirect_to request.referer, notice: 'Your booking has been cancelled successfully.'
  end

  def student_profile
    @student = User.find_by(id: params[:id])
  end

  def send_payment_instructions
    if @booking.update_attribute(:status, "payment_instructions_sent")
      create_payment_instructions_sent_state_messages(@booking)
      redirect_to request.referer, notice: 'Payment instructions have been sent successfully.'
    end
  end

  # |<---------Private methods---------->|
  private
  def set_lesson_data
    @ls = LessonSchedule.find_by(id: params[:lesson_schedules][:ls_id])
    @lesson = @ls.lesson
  end
  
  def check_if_booking_is_applicable
    booking_applicable = true
    if @lesson.instructor == current_user
      booking_applicable = false
    elsif Booking.find_by({lesson_id: @lesson.id, instructor_id: @lesson.user.id, status: 'review_left'}).present?   
      booking_applicable = false
    end
  end

  def build_chat
    @chat = Chat.new({sender_id: current_user.id, recipient_id: @lesson.user.id, booking_id: @booking.id, lesson_id: @ls.lesson.id })
  end

  def build_booking
    @booking = Booking.new({lesson_schedule_id: @ls.id, instructor_id: @lesson.user.id, student_id: current_user.id, lesson_amount: @ls.rate.to_f, lesson_id: @lesson.id, status: 'pending'})
  end

  def create_message(sender_id, recipient_id, chat_id, content)
    Message.create({content: content, sender_id: sender_id, recipient_id: recipient_id, chat_id: chat_id, read: false })
  end

  def set_booking
    @booking = Booking.find_by(id: params[:id])
  end

  def create_accepted_state_messages(booking)
    message_content = "#{booking.instructor.display_name} has accepted your booking request."
    create_message(booking.instructor_id, booking.student_id, booking.chat.id, message_content)
  end

  def create_confirmed_state_messages(booking)
    if inst_pay_me(booking).present? && !inst_pay_me(booking).try(:is_pay_me_na?)
      message_content = "#{booking.instructor.display_name} has confirmed your payment."
      create_message(booking.instructor_id, booking.student_id, booking.chat.id, message_content)
    end
    message_content = "#{booking.instructor.display_name} has confirmed your booking request."
    create_message(booking.instructor_id, booking.student_id, booking.chat.id, message_content)
  end

  def create_payment_instructions_sent_state_messages(booking)
    create_message(booking.instructor_id, booking.student_id, booking.chat.id, payment_details_message_content(booking))
  end

  def payment_details_message_content(booking)
    if inst_pay_me(booking).is_pay_me_bank_transfer?
      "Please transfer the amount to #{inst_pay_me(booking).detail}"
    elsif inst_pay_me(booking).is_pay_me_webpage?
      "Please proceed to #{inst_pay_me(booking).detail} for payment details."
    elsif inst_pay_me(booking).is_pay_me_cash?
      "Payment will be done at the end of the lesson. We accept cash payments only."
    end
  end

  def create_pending_state_messages(booking, chat=nil)
    message_content = "#{current_user.display_name} has requested for a lesson: #{booking.lesson_schedule.lesson_date}, #{booking.lesson_schedule.from_time.strftime('%r')}-#{booking.lesson_schedule.to_time.strftime('%r')}."
    create_message(booking.student_id, booking.instructor_id, chat.id, message_content)
  end

  def create_cancel_state_messages(booking)
    message_content = "#{booking.student.display_name} has cancelled the booking request"   
    create_message(booking.student_id, booking.instructor_id, booking.chat.id, message_content)
  end

  def inst_pay_me(booking)
    booking.instructor.payment_method
  end

  def create_declined_state_messages(booking)
    create_message(booking.instructor_id, booking.student_id, booking.chat.id, "#{booking.instructor.display_name} has rejected your booking request.")
  end

  def flash_messages_on_perform_action(action)
    if action == 'accepted'
      return "Selected bookings were accepted successfully."
    elsif action == 'confirmed'
      return "Selected bookings were confirmed successfully."
    elsif action == 'declined'
      return "Selected bookings were declined successfully."
    elsif action == 'delete'
      return "Selected bookings were deleted successfully."
    elsif action == 'payment_instructions_sent'
      return "Payment instructions successfully sent to selected bookings."  
    end
  end
end