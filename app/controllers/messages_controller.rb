class MessagesController < ApplicationController
  def create
    message = Message.new(message_params)
  	if message.save
  		redirect_to request.referer
  	end
  end

  def update_chat_box
    @chat = Chat.find_by(id: params[:id])
    @messages = @chat.messages.select{|m| m.id > params[:last_message_id].to_i}
    
    respond_to do |format|
      format.js
    end
  end

  private
  def message_params
  	params.require(:message).permit(:sender_id, :recipient_id, :chat_id, :content, :attachment, :read)
  end

end
