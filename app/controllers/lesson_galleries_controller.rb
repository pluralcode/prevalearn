class LessonGalleriesController < ApplicationController
	before_action :authenticate_user!

	def destroy
		@lesson_gallery = LessonGallery.find(params[:id])
		@lesson_gallery.destroy

		respond_to do |format|
			format.html { redirect_to request.referer, notice: "Photo successfully removed" }
		end
	end
end