class AccountsController < ApplicationController
	before_action :authenticate_user!

	def update
		respond_to do |format|
			begin
				if !params[:individual_info].nil?
					logger.info params[:individual_info].inspect
					params[:individual_info]

					if params[:individual_info][:full_name]
						names = params[:individual_info][:full_name].gsub(" ", ",").split(',')
						first_name = names[0]
						names.delete_at(0)
						last_name =  names.join(' ')
						current_user.profile.profilable.update_attributes(
							first_name: first_name, last_name: last_name
						)
					elsif params[:individual_info][:email]
						current_user.update_attributes(
							email: params[:individual_info][:email]
						)
					end

					current_user.profile.profilable.update_attributes(
						display_name: params[:individual_info][:display_name],
						gender_id: params[:individual_info][:gender],
						contact_number: params[:individual_info][:mobile],
						date_of_birth: params[:individual_info][:date_of_birth],
						address: params[:individual_info][:address],
						postal_code: params[:individual_info][:zipcode]
					)
				elsif !params[:organization_info].nil?
					if params[:organization_info][:email]
						current_user.update_attributes(
							email: params[:organization_info][:email]
						)
					end

					if params[:organization_info][:about]
						current_user.profile.profilable.about = params[:organization_info][:about]
						current_user.profile.profilable.save validate: false
					end

				 	current_user.profile.profilable.update_attributes(
				 		name: params[:organization_info][:name],
				 		type_of_company: params[:organization_info][:type_of_company],
				 		address: params[:organization_info][:address],
				 		zipcode: params[:organization_info][:zipcode],
				 		website: params[:organization_info][:website]
					)

				end

				format.html { redirect_to dashboard_url, notice: "Details successfully updated" }
			rescue StandardError => e
				raise e
				flash[:error] = "An error occured. Please try again"
				format.html { redirect_to dashboard_url }
			end
		end
	end

	def verify_account
		@user = current_user
	end

	def confirm_verification
		code = params[:user][:verification_code]

		if code.eql?(current_user.confirmation_token)
			current_user.update_attributes(
				confirmed_at: DateTime.current, confirmation_token: nil)
			redirect_to dashboard_url, notice: "Account successfully verified"
		else
			flash[:error] = "Invalid verification code"
			redirect_to dashboard_url
		end
	end

	def delete_account
		@user = current_user
	end

	def confirm_delete
		respond_to do |format|
			begin
		    	bcrypt_salt = User::BCrypt::Password.new(current_user.try(:encrypted_password)).salt
		    	bcrypt_password_hash = User::BCrypt::Engine.hash_secret(
		    		"#{params[:user][:password]}#{current_user.class.pepper}", bcrypt_salt)
		    	password_check = Devise.secure_compare(bcrypt_password_hash, current_user.try(:encrypted_password))

		    	if password_check
		    		@success = "You typed on the RIGHT password"
		    		format.js {}
		    	else
		    		@failure = "You typed on the WRONG password"
		    		format.js {}
		    	end
			rescue StandardError => e
				raise e
				flash[:error] = "An error occured"
				redirect_to dashboard_url
			end
		end
	end

	def problem_report
	end

	def email_report
		respond_to do |format|
			begin
				email = current_user.email
				content = params[:report][:desc]
				ReportMailer.email_report(email, content).deliver
				@success = "Thank you for reporting problem"
				format.js {}
			rescue Net::SMTPAuthenticationError, Net::SMTPServerBusy, Net::SMTPSyntaxError, Net::SMTPFatalError, Net::SMTPUnknownError => e
				raise e
				flash[:error] = "An error occured"
				redirect_to dashboard_url
			end
		end
	end

	def payment
		# @current_user = params[:format]
		@param = PayMe.new(payment_param)

			if @param.save
				redirect_to "/dashboard", notice: 'Your payment setting was updated.'
			else
				redirect_to '/dashboard', notice: 'There was an error and your payment setting was not saved. Try again.'
			end
	end

	private

	def payment_param
		payment_detail_val
		params.require(:pay_me).permit(:method, :detail, :user)
	end

	def payment_detail_val
		params[:pay_me][:detail] = params[:pay_me][:method] == 'webpage' ? params[:pay_me][:payment_website] : params[:pay_me][:bank_instructions]
	end
end
