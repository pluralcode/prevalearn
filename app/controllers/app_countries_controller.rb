class AppCountriesController < ApplicationController	
	def show
		@country = AppCountry.find(params[:id])

		respond_to do |format|
			format.json { render json: @country, status: :ok }
		end
	end
end
