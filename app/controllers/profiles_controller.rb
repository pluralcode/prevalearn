class ProfilesController < ApplicationController
	before_action :set_profile
	skip_before_action :verify_authenticity_token

	def update
		respond_to do |format|
			if @profile.update(profile_params)
				format.html { redirect_to dashboard_url, notice: 'Successfully updated.' }
				format.json { render :show, status: :ok, location: @profile }
			else
				message = "An error occured while updating. Please try again"
				flash[:error] = message
				format.html { redirect_to @dashboard_url }
				format.json { render json: { error: message }, status: :unprocessable_entity }
			end
		end
	end

	private
		def profile_params
			params.require(:profile).permit(:avatar)
		end

		def set_profile
			@profile = Profile.find(params[:id])
		end

end
