class DashboardController < ApplicationController
	before_action :authenticate_user!

	def index
		# the inital admin account does not have a profile to load for the dashboard
		# will likely need a real solution in the near future
    @pay_me = current_user.payment_method if !current_user.is_admin

    return redirect_to :admin_users if current_user.is_admin
	end

	helper_method :resource_name, :resource, :devise_mapping

  	def resource_name
    	:user
  	end

  	def resource
    	@resource ||= User.new
  	end

  	def devise_mapping
    	@devise_mapping ||= Devise.mappings[:user]
  	end
end
