class ApplicationController < ActionController::Base
	# Prevent CSRF attacks by raising an exception.
	# For APIs, you may want to use :null_session instead.
	protect_from_forgery with: :exception
	before_action :profile_exists?
	geocode_ip_address

	def after_sign_in_path_for(resource)
		if resource.is_admin
			admin_root_path
		else
			super
		end
	end

	private
		def profile_exists?
			if current_user && current_user.profile.nil?
				unless current_user.is_admin
					redirect_to after_signup_path(:step_one)
				end
			end
		end
end
