class StaticPagesController < ApplicationController
  def home
  end

  def teach_with_us
  end

  def how_it_works
  end

  def contact_us
  end

  def about_us
  end

  def faq
  end

  def privacy
  end

  def terms

  end

  def report_problem
    render json:'hello'
  end
end
