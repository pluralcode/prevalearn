class ReportedUsersController < ApplicationController
  def create
    @reported_user = ReportedUser.new(reported_user_params)
    @reported_user.save

    respond_to do |format|
      format.js
    end
  end

  private
  def reported_user_params
    params.require(:reported_user).permit(:reporter_id, :reportee_id, :content)
  end
end
