class RegistrationsController < Devise::RegistrationsController
	def update
		self.resource = resource_class.to_adapter.get!(send(:"current_#{resource_name}").to_key)
		prev_unconfirmed_email = resource.unconfirmed_email if resource.respond_to?(:unconfirmed_email)

		resource_updated = update_resource(resource, account_update_params)
		yield resource if block_given?
		if resource_updated
			if is_flashing_format?
				flash_key = update_needs_confirmation?(resource, prev_unconfirmed_email) ? :update_needs_confirmation : :updated
				set_flash_message :notice, flash_key
			end
			unless resource.is_admin
				bypass_sign_in resource, scope: resource_name
			end

			respond_with resource, location: after_update_path_for(resource)
    	else
    		flash[:error] = "Password changed failed. Please try again"
    		clean_up_passwords resource

    		if resource.is_admin
    			respond_with resource
    		else
    			redirect_to dashboard_url
    		end
    	end
    end

	protected
		def after_sign_up_path_for(resource)
			after_signup_index_path
		end

		def after_update_path_for(resource)
			if resource.is_admin
				admin_root_path
			else
				dashboard_url
			end
    end
end
