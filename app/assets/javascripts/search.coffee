jQuery ->
  $('.ui-autocomplete-input').autocomplete({
    minLength: 1
    delay: 50
    source: (request, response) ->
      $.ajax(
        url: $('#search')[0].action
        method: $('#search')[0].method
        dataType: 'JSON'
        data:
          search:
            search: request.term
          limit: 6
        success: (data) ->
          response($.map(data, (item) ->
            code = item.lesson_categories[0].category.code.toLowerCase()
            return {
              label: item.title.slice(0, 35)
              icon: "/assets/categories/" + code + '.png'
              code: code
              value: item.title
              id: item.id
            }
          ))
      )
    select: (event, ui) ->
      document.location = "/lessons/" + ui.item.id
    open: () ->
      $( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" )
    close: () ->
      $( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" )
  }).autocomplete('instance')._renderItem = (ul, item) ->
      return $( "<li class='ui-menu-item-with-icon'>" )
              .attr( "data-value", item.value )
              .append("<span><img src='" +item.icon+ "' height='17.5' width='17.5' alt='"+item.code+"'></span>" + item.label)
              .appendTo( ul )

if $('.search_simple_form').is(':visible')
  alert('hello world fucker')
