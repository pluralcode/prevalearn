jQuery ->
	$('.chosen-select').chosen()

	$(".individual_info_gender").find("label.radio_buttons").addClass("col-sm-3")
	$(".individual_info_gender").find(".radio").wrapAll("<div class='col-sm-9'>")

	$(".upload").on "click", (e) ->
		$("#uploadAvatar").click()

	$("input#uploadAvatar[type=file]").on "change", (e) ->
		readUrl(this)
		$(this).closest('form').submit()

	readUrl = (input) ->
		if input.files && input.files[0]
			reader = new FileReader()

			reader.onload = (e) ->
				$(".avatar > img").attr("src", e.target.result)

			reader.readAsDataURL(input.files[0])


	$("#transfer_textrea").hide()
	$("#webpage_payment").hide()

	$('#transfer_textrea').show() if $('#bank_transfer_button').is(':checked')
	$('#webpage_payment').show() if $('#webpage_detail_button').is(':checked')

	$("#bank_transfer_button").on "click", () ->
		$("#transfer_textrea").toggle()
		$("#webpage_payment").hide()

	$("#webpage_detail_button").on "click", () ->
		$("#webpage_payment").toggle()
		$("#transfer_textrea").hide()

	$("#cash_payment_button").on "click", () ->
		$("#transfer_textrea").hide()
		$("#webpage_payment").hide()

	$("#na_payment_button").on "click", () ->
		$("#transfer_textrea").hide()
		$("#webpage_payment").hide()
