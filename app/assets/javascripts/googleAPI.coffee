jQuery ->

  geocoder = new (google.maps.Geocoder)

  pos = {}

  auto_loc = ->
    input = $("#location")[0]
    places = new (google.maps.places.Autocomplete)(input)
    return

  google.maps.event.addDomListener window, "load", auto_loc ->
    $(document).bind 'projectLoadComplete', auto_loc

  $('#location').focusout ->
    address = $('#location').val()
    geoCode(address)
    return

  if (navigator.geolocation)
    navigator.geolocation.getCurrentPosition (position) ->
      pos =
        lat: position.coords.latitude
        lng: position.coords.longitude
      reverseGeoCode pos
      pos
  else
    pass

  reverseGeoCode = (locs) ->
    geocoder.geocode {"location": locs}, (res, stat) ->
      if stat == google.maps.GeocoderStatus.OK
        $(".browse_from").val res[0].formatted_address
        locs = res[0].geometry.location
        $("#code_lat").val(locs.lat())
        $("#code_lng").val(locs.lng())
      else
        # verify googleAPI server res
      return
    return

  geoCode = (address) ->
    geocoder.geocode {"address": address}, (res, stat) ->
      if stat == google.maps.GeocoderStatus.OK
       locs = res[0].geometry.location
       $("#code_lat").val(locs.lat())
       $("#code_lng").val(locs.lng())
      else
       # verify googleAPI server res
      return
    return
