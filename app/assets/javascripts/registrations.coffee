jQuery ->
	$("#individual_profile_attributes_app_country_id").on 'change', () ->
		countryId = $(this).val()
		$.ajax
			url: "/app_countries/#{countryId}"
			type: "GET"
			dataType: "JSON"
			success: (data, response) ->
				$("#individual_contact_number")
					.attr("placeholder", "+#{data.country_code}...")
