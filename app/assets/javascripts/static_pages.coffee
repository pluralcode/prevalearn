jQuery ->
	$("#general").css("font-weight", "bold")
	$("#provider-content").hide()
	$("#seeker-content").hide()

	$("#general").on "click", (event) ->
		$(this).css("font-weight", "bold")
		$("#general-content").show()

		$("#provider").css("font-weight", "")
		$("#provider-content").hide()

		$("#seeker").css("font-weight", "")
		$("#seeker-content").hide()

	$("#seeker").on "click", (event) ->
		$(this).css("font-weight", "bold")
		$("#seeker-content").show()

		$("#general").css("font-weight", "")
		$("#general-content").hide()

		$("#provider").css("font-weight", "")
		$("#provider-content").hide()

	$("#provider").on "click", (event) ->
		$(this).css("font-weight", "bold")
		$("#provider-content").show()

		$("#general").css("font-weight", "")
		$("#general-content").hide()

		$("#seeker").css("font-weight", "")
		$("#seeker-content").hide()
