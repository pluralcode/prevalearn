jQuery ->
	$(".new-showcase").on "click", (e) ->
		$("#lesson_gallery").click()

	$(".showcase-upload").on "click", (e) ->
		$("#lesson_gallery").click()

	$("input#lesson_gallery[type=file]").on "change", (e) ->
		readGalleryUrl(this)

	readGalleryUrl = (input) ->
		if input.files && input.files[0]
			reader = new FileReader()

			reader.onload = (e) ->
				$(".display-box > img").attr("src", e.target.result)

			reader.readAsDataURL(input.files[0])

	$(".showcase").on "click", (e) ->
		imgSrc = $(this).find('img').attr("src")
		$(".display-box > img").attr("src", imgSrc)

	$('form').on 'click', '.add_fields', (event) ->
		time = new Date().getTime()
		regexp = new RegExp($(this).data('id'), 'g')
		$(this).before($(this).data('fields').replace(regexp, time))

		$('.hasDatePicker').removeClass('date_picker').addClass('datepicker')
		$('.datepicker').datetimepicker()
		$('.hasTimePicker').removeClass('time_picker').addClass('timepicker')
		$('.timepicker').datetimepicker()

		event.preventDefault()

	$('form').on 'click', '.remove_fields', (event) ->
		$(this).prev().find("input[type='hidden']").val(1)
		$(this).parent().hide()
		event.preventDefault()

	$('form').on 'change', '.category', (event) ->
		klass = "category"
		objectId = $(this).val()

		select_change_call(klass, objectId)

	$('form').on 'change', '.sub_category', (event) ->
		klass = "sub_category"
		objectId = $(this).val()

		select_change_call(klass, objectId)

	select_change_call = (klass, objectId) ->
		$.ajax
			url: '/select_change'
			type: 'GET'
			data:
				klass: klass
				object_id: objectId
			dataType: 'JSON'
			success: (response) ->
				if klass is "category"
					targetNode = $('select.sub_category')
					targetNode.empty()
					$('select.sub_sub_category').empty()

					targetNode.append("<option value='#{cat.id}'>#{cat.name}</option>") for cat in response
				if klass is "sub_category"
					targetNode = $('select.sub_sub_category').empty()
					targetNode.append("<option value='#{cat.id}'>#{cat.name}</option>") for cat in response
