# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

() ->
	$('.accept-payment-button').on "click", ()->
		$('#accept-booking-confirmation').modal('show');

$("input#lesson_gallery[type=file]").on "change", (e) ->
    readGalleryUrl(this)

  readGalleryUrl = (input) ->
    if input.files && input.files[0]
      reader = new FileReader()

      reader.onload = (e) ->
        $(".display-box > img").attr("src", e.target.result)

      reader.readAsDataURL(input.files[0])    