class ReportMailer < ApplicationMailer
  def email_report(email, content)
    @email = email
    @content = content

    mail(from: email, subject: "Report Problem")
  end
end
