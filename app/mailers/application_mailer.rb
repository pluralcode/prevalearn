class ApplicationMailer < ActionMailer::Base
  default to: "support@prevalearn.com"
  layout 'mailer'
end
