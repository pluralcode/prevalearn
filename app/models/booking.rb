class Booking < ActiveRecord::Base

  STATUSES = ['pending', 'accepted', 'declined', 'completed', 'cancelled', 'review_left', 'confirmed', 'payment_instructions_sent']

  scope :having_statuses, lambda { |statuses| where(status: statuses) }

  has_one :chat
  belongs_to :lesson_schedule
  belongs_to :lesson
  belongs_to :instructor, class_name: User
  belongs_to :student, class_name: User


  validates_presence_of :status, :instructor_id, :student_id

  validates :status, inclusion: {in: STATUSES }

  STATUSES.each do |booking_status|
    define_method("#{booking_status}?") do
      self.status == booking_status
    end
  end

  def self.status_due_to_payme(inst_pay_me)
    status = if inst_pay_me.present? 
      inst_pay_me.is_pay_me_na? ? 'confirmed' : 'accepted'
    else
      'confirmed'
    end
    return status
  end

end