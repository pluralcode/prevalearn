class Review < ActiveRecord::Base

	belongs_to :reviewer, class_name: User
	belongs_to :lesson

	validates_presence_of :title, :rating, :reviewer_id, :reviewee_id
end
