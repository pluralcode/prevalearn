class Chat < ActiveRecord::Base
  scope :sender_chats, lambda { |sender_id| where({sender_id: sender_id, deleted_by_sender: false}) }
  scope :recipient_chats, lambda { |recipient_id| where({recipient_id: recipient_id, deleted_by_recipient: false}) }

	belongs_to :sender, class_name: User
	belongs_to :recipient, class_name: User
	belongs_to :booking
	belongs_to :lesson
	has_many :messages, dependent: :destroy

	validates_presence_of :sender_id, :recipient_id

	def is_chat_has_unread_messages?
		messages.present? && messages.select{|m| !m.read?}.present?
	end

  ['sender', 'recipient'].each do |ut|
    define_method("#{ut}?".to_sym) do |user|
      self.send(ut) == user
    end
  end
end
