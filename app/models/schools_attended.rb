class SchoolsAttended < ActiveRecord::Base
	belongs_to :qualification
	belongs_to :lesson, inverse_of: :schools_attendeds
end
