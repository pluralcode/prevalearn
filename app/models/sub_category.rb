class SubCategory < ActiveRecord::Base
	belongs_to :category, inverse_of: :sub_categories

	has_many :lessons, inverse_of: :sub_category
	has_many :sub_sub_categories, inverse_of: :sub_category
	has_many :lesson_categories, inverse_of: :sub_category
end
