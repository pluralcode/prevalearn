class AgeRequirement < ActiveRecord::Base
	has_many :lesson_specifications, inverse_of: :age_requirement
end
