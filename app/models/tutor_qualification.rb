class TutorQualification < ActiveRecord::Base
	belongs_to :lesson, inverse_of: :tutor_qualifications
end
