class PayMe < ActiveRecord::Base

  validates_presence_of :user, :method
  validates :detail, presence: true, if: 'method == "bank_transfer" || method == "webpage"'

  ['webpage', 'bank_transfer', 'cash', 'na'].each do |pay_method|
    define_method("is_pay_me_#{pay_method}?") do
      self.method == pay_method
    end
  end
end
