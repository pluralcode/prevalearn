class Award < ActiveRecord::Base
	belongs_to :lesson, inverse_of: :awards
end
