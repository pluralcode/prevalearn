class Category < ActiveRecord::Base
	has_many :lessons, inverse_of: :category
	has_many :sub_categories, inverse_of: :category
	has_many :sub_sub_categories, through: :sub_categories
	has_many :lesson_categories, inverse_of: :category
end
