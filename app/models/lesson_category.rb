class LessonCategory < ActiveRecord::Base
	belongs_to :lesson, inverse_of: :lesson_categories
	belongs_to :category, inverse_of: :lesson_categories
	belongs_to :sub_category, inverse_of: :lesson_categories
	belongs_to :sub_sub_category, inverse_of: :lesson_categories
end
