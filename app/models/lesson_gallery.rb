class LessonGallery < ActiveRecord::Base
	belongs_to :lesson, inverse_of: :lesson_galleries

	has_attached_file :photo, styles: { medium: "300x200>", thumb: "100x100>" }, default_url: ""
	validates_attachment_content_type :photo, content_type: /\Aimage\/.*\Z/
	validates_attachment_size :photo, in: 0.megabytes..5.megabytes
end
