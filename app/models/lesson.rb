class Lesson < ActiveRecord::Base
    belongs_to :user, inverse_of: :lessons
    belongs_to :category, inverse_of: :lessons
    belongs_to :sub_category, inverse_of: :lessons
    belongs_to :sub_sub_category, inverse_of: :lessons

    has_one :lesson_specification, inverse_of: :lesson, dependent: :destroy
    accepts_nested_attributes_for :lesson_specification, allow_destroy: true

    has_one :tutor_qualification, inverse_of: :lesson, dependent: :destroy
    accepts_nested_attributes_for :tutor_qualification, allow_destroy: true

    has_many :lesson_schedules, inverse_of: :lesson, dependent: :destroy
    accepts_nested_attributes_for :lesson_schedules, allow_destroy: true

    has_many :lesson_galleries, inverse_of: :lesson, dependent: :destroy
    accepts_nested_attributes_for :lesson_galleries, allow_destroy: true

    has_many :lesson_categories, inverse_of: :lesson, dependent: :destroy
    accepts_nested_attributes_for :lesson_categories, allow_destroy: true

    has_many :awards, inverse_of: :lesson, dependent: :destroy
    accepts_nested_attributes_for :awards, allow_destroy: true

    has_many :schools_attendeds, inverse_of: :lesson, dependent: :destroy
    accepts_nested_attributes_for :schools_attendeds, allow_destroy: true

    has_one :tutor_qualification, dependent: :destroy
    accepts_nested_attributes_for :tutor_qualification, allow_destroy: true

    has_one :profile, through: :user

    validates :title, presence: true
    validates_length_of :title, :maximum => 50

    has_many :reviews

    def price_range
        rates = self.lesson_schedules.pluck(:rate).uniq

        {
            multiple_rates: rates.size > 1,
            lowest: rates.min,
            rates: rates
        }
    end

    def lesson_price
    prices = self.price_range
    if !prices[:lowest] 
      "Contact instructor for pricing"
    elsif prices[:lowest] == prices[:rates].max 
      prices[:lowest] 
    else
      "#{prices[:lowest]}-#{prices[:rates].max}"
    end
  end

    def show_school
        school_name = self.schools_attendeds.pluck(:name).select { |s| !s.strip.empty? }

        school_name.empty? ? "NA" : school_name.first
    end

    def lesson_individual?
        self.user.profile.user_type.code == "INDIVIDUAL"
    end

end
