class Message < ActiveRecord::Base
  
  has_attached_file :attachment, styles: { medium: "300x300>", thumb: "100x100>" }, default_url: "/images/:style/missing.png"
  validates_attachment_content_type :attachment, content_type: /\Aimage\/.*\z/

  belongs_to :chat
  belongs_to :sender, class_name: User
  belongs_to :recipient, class_name: User

  validates_presence_of :sender_id, :recipient_id, :chat_id
  validates :content, presence: true, if: 'attachment.blank?'
  validates :attachment, presence: true, if: 'content.blank?'

end
