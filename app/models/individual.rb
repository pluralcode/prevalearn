class Individual < ActiveRecord::Base
	belongs_to :gender
	belongs_to :objective

	has_one :profile, as: :profilable, dependent: :destroy
	accepts_nested_attributes_for :profile, allow_destroy: true

	attr_accessor :step

	validates :contact_number, numericality: { only_integer: true }, if: :id_present?
	validates :first_name, :last_name, presence: true, if: :step_three?
	validates :objective, :gender, :date_of_birth, presence: true, if: :step_four?

	def id_present?
		self.id.present?
	end

	def step_three?
		step.eql?("step_three")
	end

	def step_four?
		step.eql?("step_four")
	end

	def complete_name
		"#{first_name} #{last_name}"
	end

end
