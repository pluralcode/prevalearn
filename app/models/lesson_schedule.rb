class LessonSchedule < ActiveRecord::Base
	belongs_to :lesson, inverse_of: :lesson_schedules
end
