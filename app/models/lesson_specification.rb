class LessonSpecification < ActiveRecord::Base
	belongs_to :lesson, inverse_of: :lesson_specification
	belongs_to :class_level, inverse_of: :lesson_specifications
	belongs_to :age_requirement, inverse_of: :lesson_specifications
end
