class Profile < ActiveRecord::Base
	belongs_to :user
	belongs_to :user_type
	belongs_to :app_country
	belongs_to :profilable, polymorphic: true

	validates :user, :user_type, presence: true
	validates :app_country, presence: true, if: :id_present?

	has_attached_file :avatar, styles: { medium: "300x300>", thumb: "100x100>" }, default_url: "/assets/avatar.png"
	validates_attachment_content_type :avatar, content_type: /\Aimage\/.*\Z/

	def id_present?
		self.id.present?
	end
end
