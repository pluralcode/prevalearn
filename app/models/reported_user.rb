class ReportedUser < ActiveRecord::Base

  belongs_to :reporter, class_name: User
  belongs_to :reportee, class_name: User

  validates_presence_of :reportee_id, :reporter_id, :content

end
