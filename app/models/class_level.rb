class ClassLevel < ActiveRecord::Base
	has_many :lesson_specifications, inverse_of: :class_level
end
