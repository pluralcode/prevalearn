class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable, :confirmable,
    :recoverable, :rememberable, :trackable, :validatable

  has_many :payments, dependent: :destroy
  has_one :profile, dependent: :destroy

  has_many :lessons, inverse_of: :user, dependent: :destroy
  has_many :lesson_categories, through: :lessons
  has_many :lesson_galleries, through: :lessons
  has_many :shortlisted_lessons

  has_many :initiated_chats, class_name: Chat, foreign_key: "sender_id"
  has_many :received_chats, class_name: Chat, foreign_key: "recipient_id"

  attr_accessor :verification_code

  def chats
    chats = Array.new.push([initiated_chats, received_chats]).flatten.uniq
  end

  def display_name
    user_profile = self.profile.try(:profilable)
    if user_type == "INDIVIDUAL"
      user_profile.try(:complete_name).present? ?  user_profile.try(:complete_name) : user_profile.try(:display_name)
    else
      user_profile.try(:name)
    end
  end

  def about
    if user_type == 'ORGANIZATION'
      profile.try(:profilable).try(:about)
    end
  end

  def gender
    if user_type == "INDIVIDUAL"
      profile.try(:profilable).try(:gender).try(:name)
    else
      "N/A"
    end
  end

  def date_of_birth
    if user_type == "INDIVIDUAL"
      profile.try(:profilable).try(:date_of_birth)
    else
      "N/A"
    end
  end

  def can_leave_review_for(lesson, reviewee)
    Review.where({reviewee_id: reviewee.id, reviewer_id: self.id, lesson_id: lesson.id}).empty?
  end

  def payment_method
    PayMe.where(user: self.id).last
  end

  def user_type
    self.profile.try(:user_type).try(:code)
  end
end
