class SubSubCategory < ActiveRecord::Base
	belongs_to :sub_category, inverse_of: :sub_sub_categories
	has_many :lessons, inverse_of: :sub_sub_category
	has_many :lesson_categories, inverse_of: :sub_sub_category
end
