class Organization < ActiveRecord::Base
	belongs_to :industry
	
	has_one :profile, as: :profilable, dependent: :destroy
	accepts_nested_attributes_for :profile, allow_destroy: true

	attr_accessor :step

	validates :name, presence: true, if: :id_present?
	validates :address, :industry, presence: true, if: :step_three?
	validates :registration_number, presence: true, if: :step_four?	
	validates :contact_number, numericality: { only_integer: true }, if: :step_four?	
	validates :website, presence: true, if: :step_four?	
	validates :website, url: true, if: "website.upcase.gsub('.', '').gsub('/', '') != 'NA'"

	def id_present?
		self.id.present?
	end

	def step_three?
		step.eql?("step_three")
	end

	def step_four?
		step.eql?("step_four")
	end

	def not_applicable?
		self.website.present? && self.website != 'NIL' || self.website != 'NA'
	end
end