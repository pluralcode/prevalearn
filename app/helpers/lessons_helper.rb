module LessonsHelper

  def lesson_gal_first_image(lesson)
    if lesson.lesson_galleries.first.nil?
      image_url('default.png')
    else
      lesson.lesson_galleries.first.photo.url
    end
  end

  def display_lesson_categories
    category = @lesson.lesson_categories.last.category
    sc = @lesson.lesson_categories.last.try(:sub_category)
    ssc = @lesson.lesson_categories.last.try(:sub_sub_category)
    if category.present?
      categories_to_display = link_to(category.try(:name), category_path(category.id))
      categories_to_display = categories_to_display + " | #{ link_to(sc.name, sub_category_path(sc.id)) }".html_safe if sc.present?   
      categories_to_display = categories_to_display + " | #{ssc.name}" if ssc.present?
      categories_to_display = categories_to_display + " | #{ link_to(@lesson.title, lesson_path(@lesson.id)) }".html_safe
    else
      categories_to_display = "#{ link_to(@lesson.title, lesson_path(@lesson.id)) }".html_safe 
    end
    categories_to_display
  end

  def inst_address
    (@instructor || @chat.try(:lesson).try(:user) || current_user).profile.try(:profilable).try(:address)
  end

  def lesson_shortlisted?
    ShortlistedLesson.find_by(user_id: current_user.id, lesson_id: @lesson.id).present?
  end

  def ls_month
    ls = @lesson.lesson_schedules.select{|ls| ls.lesson_date >= Date.today}.first
    ls.present? ? ls.lesson_date.strftime("%b") : "N/A"
  end

  def ls_date
    ls = @lesson.lesson_schedules.select{|ls| ls.lesson_date >= Date.today}.first
    ls.present? ? ls.lesson_date.strftime("%d") : "N/A"
  end

  def ls_day
    ls = @lesson.lesson_schedules.select{|ls| ls.lesson_date >= Date.today}.first
    ls.present? ? ls.lesson_date.strftime("%A") : "N/A"
  end

  def ls_time_slot
    ls = @lesson.lesson_schedules.select{|ls| ls.lesson_date >= Date.today}.first
    if ls.present?
      "#{ls.from_time.strftime('%H:%M')} - #{ls.to_time.strftime('%H:%M')}"
    else
      "N/A"
    end
  end

  def create_booking_path
    @chat.nil? ? booking_with_chat_path(@lesson.id) : booking_without_chat_path(@chat.lesson.id, chat_id: @chat.id)
  end

  def lesson_schedules
    lesson = (@lesson || @chat.lesson)
    requested_lesson_schedules = Booking.where({lesson_id: lesson.id, status: ['pending', 'accepted', 'confirmed'], student_id: current_user.id}).map(&:lesson_schedule_id)
    lesson.lesson_schedules.select{|ls| ls.lesson_date >= Date.today && !requested_lesson_schedules.include?(ls.id) }
  end

  def book_lesson_btn_class
    "btn #{lesson_schedules.present? ? 'btn-success' : 'btn-default'} modal-center-button book-lesson-button"
  end

  def inst_classes_in_same_category
    Lesson.where(user_id: @instructor.id).select{|l| l != @lesson && l.lesson_categories.include?(@lesson.lesson_categories.last.category)}
  end

  def related_lessons
    LessonCategory.where(category_id: @lesson.lesson_categories.last.category.try(:id)).map{|lc| lc.lesson}.select{|l| l != @lesson }
  end
end
