module ApplicationHelper
	def flash_messages(opts={})
		@layout_flash = opts.fetch(:layout_flash) { true }

		capture do
			flash.each do |name, msg|
				concat content_tag(:div, msg, id: "flash-#{name}")
			end
		end
	end

	def show_layout_flash?
		@layout_flash.nil? ? true : @layout_flash
	end

	def confirmation_status(user)
		confirmed = user.confirmed?

		case confirmed
		when true
			if user.pending_reconfirmation?
				"Unverified #{link_to 'Verify now', verify_account_accounts_path, remote: true}"
			else
		 		"Verified"
		 	end
		when false
			"Unverified #{link_to 'Verify now', verify_account_accounts_path, remote: true}"
		end
	end

	def link_to_add_fields(name, f, association)
		new_object = f.object.send(association).klass.new
		id = new_object.object_id
		fields = f.simple_fields_for(association, new_object, child_index: id) do |builder|
			render(association.to_s.singularize + "_fields", f: builder)
		end
		link_to('#', class: 'add_fields', data: { id: id, fields: fields.gsub("\n", "")}) do
			fa_icon 'plus', text: name
		end
	end

	def display_lesson_price(lesson)
    prices = lesson.price_range
    if !prices[:lowest] 
      "Contact instructor for pricing"
    elsif prices[:lowest] == prices[:rates].max 
      number_to_currency(prices[:lowest]) 
    else 
      "#{number_to_currency(prices[:lowest])}-#{number_to_currency(prices[:rates].max)}" 
    end 
  end


  def average_reviews(lesson)
    inst_reviews = lesson.reviews.select{|r| r.lesson_id == lesson.id }
    if inst_reviews.present?
      (inst_reviews.map(&:rating).sum.to_f / inst_reviews.count.to_f).round(1)
    else
      0
    end
  end
end
