module BookingsHelper

  def manage_booking_options(requests)
    pending_requests = requests.select{|b| b.pending?}
    declined_requests = requests.select{|b| b.declined?}
    accepted_requests = requests.select{|b| b.accepted?}
    payment_instructions_sent_requests = requests.select{|b| b.payment_instructions_sent?}

    options = [['Export to CSV', 'export_to_csv']]

    inst_pay_me = current_user.payment_method
    accept_status = Booking.status_due_to_payme(inst_pay_me)
    
    [['Decline', 'declined'], ['Accept', accept_status]].each {|o| options.unshift(o)} if pending_requests.present?
    options << ['Delete', 'delete'] if pending_requests.present? || declined_requests.present?
    options << ['Send Payment Details', 'payment_instructions_sent'] if accepted_requests.present? 
    options << ['Confirm Payment', 'confirmed'] if payment_instructions_sent_requests.present?

    return options
  end

  def pending_bookings(bookings)
    bookings.select{|b| b.pending?}
  end

  def status_image_tooltip_message(request)
    if request.pending?
      "Pending Accept/Request of Lesson"
    elsif request.accepted?
      "Lesson Request Accepted"
    elsif request.payment_instructions_sent?
      "Payment Details Sent"
    elsif request.confirmed?
      "Payment received"
    elsif request.declined?
      "Lesson Request Rejected"
    end          
  end

  def inst_pay_me(booking)
    booking.instructor.payment_method
  end

  def booking_status_image(booking)
    (booking.confirmed? && inst_pay_me(booking).is_pay_me_na?) ? 'accepted' : booking.status
  end

  def booking_status_image_margin(booking)
    ((!booking.confirmed?) || (booking.confirmed? && inst_pay_me(booking).is_pay_me_na?)) ? 'margin-left: 28%;' : ''
  end
end
