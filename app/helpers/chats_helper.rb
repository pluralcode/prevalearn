module ChatsHelper

  def chat_ls(chat)
    chat.booking.try(:lesson_schedule)
  end

  def message_type(message)
    message.sender_id == current_user.id ? 'sent' : 'received'
  end

  def chat_members
    [@chat.sender, @chat.recipient]
  end

  def message_user_image(message_type)
    if message_type == 'sent'
      current_user.profile.try(:avatar).try(:url, :medium) || 'avatar.png'
    else
      chat_title_bar_user.profile.try(:avatar).try(:url, :medium) || 'avatar.png'
    end
  end

  def lesson_inst_payme
    @chat.booking.instructor.payment_method
  end

  def chat_title_bar_user
    chat_members.select{|m| m != current_user}.first
  end

  def chat_star_status(chat)
    chat.send(chat_star_field(chat))
  end

  def chat_star_field(chat)
    chat.sender?(current_user) ? :starred_by_sender : :starred_by_recipient
  end

  def other_member_in_chat(chat)
    chat_members.select{|m| m != current_user}.first
  end
end
