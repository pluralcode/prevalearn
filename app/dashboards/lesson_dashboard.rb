require "administrate/base_dashboard"

class LessonDashboard < Administrate::BaseDashboard
  # ATTRIBUTE_TYPES
  # a hash that describes the type of each of the model's fields.
  #
  # Each different type represents an Administrate::Field object,
  # which determines how the attribute is displayed
  # on pages throughout the dashboard.
  ATTRIBUTE_TYPES = {
    user: Field::BelongsTo,
    category: Field::BelongsTo,
    sub_category: Field::BelongsTo,
    sub_sub_category: Field::BelongsTo,
    lesson_specification: Field::HasOne,
    lesson_schedules: Field::HasMany,
    lesson_galleries: Field::HasMany,
    id: Field::String.with_options(searchable: false),
    title: Field::String,
    about_tutor: Field::Text,
    class_description: Field::Text,
    has_schedule: Field::Boolean,
    created_at: Field::DateTime,
    updated_at: Field::DateTime,
  }.freeze

  # COLLECTION_ATTRIBUTES
  # an array of attributes that will be displayed on the model's index page.
  #
  # By default, it's limited to four items to reduce clutter on index pages.
  # Feel free to add, remove, or rearrange items.
  COLLECTION_ATTRIBUTES = [
    :user,
    :category,
    :sub_category,
    :sub_sub_category,
  ].freeze

  # SHOW_PAGE_ATTRIBUTES
  # an array of attributes that will be displayed on the model's show page.
  SHOW_PAGE_ATTRIBUTES = [
    :user,
    :category,
    :sub_category,
    :sub_sub_category,
    :lesson_specification,
    :lesson_schedules,
    :lesson_galleries,
    :id,
    :title,
    :about_tutor,
    :class_description,
    :has_schedule,
    :created_at,
    :updated_at,
  ].freeze

  # FORM_ATTRIBUTES
  # an array of attributes that will be displayed
  # on the model's form (`new` and `edit`) pages.
  FORM_ATTRIBUTES = [
    :user,
    :category,
    :sub_category,
    :sub_sub_category,
    :lesson_specification,
    :lesson_schedules,
    :lesson_galleries,
    :title,
    :about_tutor,
    :class_description,
    :has_schedule,
  ].freeze

  # Overwrite this method to customize how lessons are displayed
  # across all pages of the admin dashboard.
  #
  # def display_resource(lesson)
  #   "Lesson ##{lesson.id}"
  # end
end
