require "administrate/base_dashboard"

class AppCountryDashboard < Administrate::BaseDashboard
  # ATTRIBUTE_TYPES
  # a hash that describes the type of each of the model's fields.
  #
  # Each different type represents an Administrate::Field object,
  # which determines how the attribute is displayed
  # on pages throughout the dashboard.
  ATTRIBUTE_TYPES = {
    profiles: Field::HasMany,
    id: Field::String.with_options(searchable: false),
    name: Field::String,
    id_code: Field::String,
    country_code: Field::String,
    currency_name: Field::String,
    currency_code: Field::String,
    currency_symbol: Field::String,
    created_at: Field::DateTime,
    updated_at: Field::DateTime,
  }.freeze

  # COLLECTION_ATTRIBUTES
  # an array of attributes that will be displayed on the model's index page.
  #
  # By default, it's limited to four items to reduce clutter on index pages.
  # Feel free to add, remove, or rearrange items.
  COLLECTION_ATTRIBUTES = [
    :id,
    :name,
    :id_code,
    :profiles
  ].freeze

  # SHOW_PAGE_ATTRIBUTES
  # an array of attributes that will be displayed on the model's show page.
  SHOW_PAGE_ATTRIBUTES = [
    :id,
    :name,
    :id_code,
    :country_code,
    :currency_name,
    :currency_code,
    :currency_symbol,
    :profiles
  ].freeze

  # FORM_ATTRIBUTES
  # an array of attributes that will be displayed
  # on the model's form (`new` and `edit`) pages.
  FORM_ATTRIBUTES = [
    :profiles,
    :name,
    :id_code,
    :country_code,
    :currency_name,
    :currency_code,
    :currency_symbol,
  ].freeze

  # Overwrite this method to customize how app countries are displayed
  # across all pages of the admin dashboard.
  #
  def display_resource(app_country)
    app_country.name
  end
end
