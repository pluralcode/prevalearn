require "administrate/base_dashboard"

class IndividualDashboard < Administrate::BaseDashboard
  # ATTRIBUTE_TYPES
  # a hash that describes the type of each of the model's fields.
  #
  # Each different type represents an Administrate::Field object,
  # which determines how the attribute is displayed
  # on pages throughout the dashboard.
  ATTRIBUTE_TYPES = {
    gender: Field::BelongsTo,
    objective: Field::BelongsTo,
    profile: Field::HasOne,
    id: Field::String.with_options(searchable: false),
    postal_code: Field::String,
    first_name: Field::String,
    last_name: Field::String,
    contact_number: Field::String,
    created_at: Field::DateTime,
    updated_at: Field::DateTime,
    date_of_birth: Field::DateTime,
    display_name: Field::String,
    address: Field::Text,
  }.freeze

  # COLLECTION_ATTRIBUTES
  # an array of attributes that will be displayed on the model's index page.
  #
  # By default, it's limited to four items to reduce clutter on index pages.
  # Feel free to add, remove, or rearrange items.
  COLLECTION_ATTRIBUTES = [
    :first_name,
    :gender,
    :objective,
    :profile,
  ].freeze

  # SHOW_PAGE_ATTRIBUTES
  # an array of attributes that will be displayed on the model's show page.
  SHOW_PAGE_ATTRIBUTES = [
    :first_name,
    :last_name,
    :gender,
    :objective,
    :profile,
    :id,
    :postal_code,
    :contact_number,
    :created_at,
    :updated_at,
    :date_of_birth,
    :display_name,
    :address,
  ].freeze

  # FORM_ATTRIBUTES
  # an array of attributes that will be displayed
  # on the model's form (`new` and `edit`) pages.
  FORM_ATTRIBUTES = [
    :gender,
    :objective,
    :profile,
    :postal_code,
    :first_name,
    :last_name,
    :contact_number,
    :date_of_birth,
    :display_name,
    :address,
  ].freeze

  # Overwrite this method to customize how individuals are displayed
  # across all pages of the admin dashboard.
  #
  def display_resource(individual)
    "#{individual.first_name} #{individual.last_name}"
  end
end
