# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160930115156) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "postgis"
  enable_extension "uuid-ossp"

  create_table "age_requirements", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
    t.string   "name"
    t.boolean  "is_active",  null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "app_countries", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
    t.string    "name"
    t.string    "id_code"
    t.string    "country_code"
    t.string    "currency_name"
    t.string    "currency_code"
    t.string    "currency_symbol"
    t.datetime  "created_at",                                                               null: false
    t.datetime  "updated_at",                                                               null: false
    t.geography "latlon",          limit: {:srid=>4326, :type=>"point", :geographic=>true}
  end

  add_index "app_countries", ["latlon"], name: "index_app_countries_on_latlon", using: :gist

  create_table "awards", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
    t.string   "name"
    t.integer  "year"
    t.uuid     "lesson_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "awards", ["lesson_id"], name: "index_awards_on_lesson_id", using: :btree

  create_table "bookings", force: :cascade do |t|
    t.string   "lesson_schedule_id"
    t.string   "instructor_id"
    t.string   "student_id"
    t.string   "payment_status"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.string   "status"
    t.float    "lesson_amount"
    t.string   "lesson_id"
  end

  create_table "categories", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
    t.string   "name"
    t.string   "code"
    t.boolean  "is_active",  default: true, null: false
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  create_table "chats", force: :cascade do |t|
    t.string   "recipient_id"
    t.string   "sender_id"
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
    t.string   "booking_id"
    t.boolean  "archived",             default: false
    t.string   "lesson_id"
    t.boolean  "starred_by_sender",    default: false
    t.boolean  "starred_by_recipient", default: false
    t.boolean  "deleted_by_sender",    default: false
    t.boolean  "deleted_by_recipient", default: false
  end

  create_table "class_levels", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
    t.string   "name"
    t.string   "code"
    t.boolean  "is_active",  null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "disciplines", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
    t.string   "name"
    t.uuid     "school_attended_id"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
  end

  add_index "disciplines", ["school_attended_id"], name: "index_disciplines_on_school_attended_id", using: :btree

  create_table "genders", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
    t.string   "name"
    t.string   "code"
    t.string   "is_active"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "individuals", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
    t.string   "postal_code"
    t.string   "first_name"
    t.string   "last_name"
    t.uuid     "gender_id"
    t.uuid     "objective_id"
    t.string   "contact_number"
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.date     "date_of_birth"
    t.string   "display_name",   default: " ", null: false
    t.text     "address",        default: " ", null: false
  end

  add_index "individuals", ["gender_id"], name: "index_individuals_on_gender_id", using: :btree
  add_index "individuals", ["objective_id"], name: "index_individuals_on_objective_id", using: :btree

  create_table "industries", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
    t.string   "name"
    t.string   "code"
    t.string   "is_active"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "lesson_categories", force: :cascade do |t|
    t.uuid     "lesson_id"
    t.uuid     "category_id"
    t.uuid     "sub_category_id"
    t.uuid     "sub_sub_category_id"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
  end

  add_index "lesson_categories", ["category_id"], name: "index_lesson_categories_on_category_id", using: :btree
  add_index "lesson_categories", ["lesson_id"], name: "index_lesson_categories_on_lesson_id", using: :btree
  add_index "lesson_categories", ["sub_category_id"], name: "index_lesson_categories_on_sub_category_id", using: :btree
  add_index "lesson_categories", ["sub_sub_category_id"], name: "index_lesson_categories_on_sub_sub_category_id", using: :btree

  create_table "lesson_galleries", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.string   "photo_file_name"
    t.string   "photo_content_type"
    t.integer  "photo_file_size"
    t.datetime "photo_updated_at"
    t.uuid     "lesson_id"
  end

  add_index "lesson_galleries", ["lesson_id"], name: "index_lesson_galleries_on_lesson_id", using: :btree

  create_table "lesson_schedules", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
    t.date     "lesson_date"
    t.time     "from_time"
    t.time     "to_time"
    t.uuid     "lesson_id"
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.decimal  "rate",        default: 0.0, null: false
  end

  add_index "lesson_schedules", ["lesson_id"], name: "index_lesson_schedules_on_lesson_id", using: :btree

  create_table "lesson_specifications", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
    t.uuid     "class_level_id"
    t.uuid     "age_requirement_id"
    t.integer  "average_class_size"
    t.text     "fee_includes"
    t.text     "class_location"
    t.uuid     "lesson_id"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.string   "number_of_lessons"
  end

  add_index "lesson_specifications", ["age_requirement_id"], name: "index_lesson_specifications_on_age_requirement_id", using: :btree
  add_index "lesson_specifications", ["class_level_id"], name: "index_lesson_specifications_on_class_level_id", using: :btree
  add_index "lesson_specifications", ["lesson_id"], name: "index_lesson_specifications_on_lesson_id", using: :btree

  create_table "lessons", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
    t.string    "title"
    t.text      "about_tutor"
    t.uuid      "category_id"
    t.uuid      "sub_category_id"
    t.uuid      "sub_sub_category_id"
    t.text      "class_description"
    t.boolean   "has_schedule",                                                                    default: false, null: false
    t.uuid      "user_id"
    t.datetime  "created_at",                                                                                      null: false
    t.datetime  "updated_at",                                                                                      null: false
    t.boolean   "is_age_public",                                                                   default: false, null: false
    t.boolean   "is_hidden_from_listing",                                                          default: false, null: false
    t.geography "latlon",                 limit: {:srid=>4326, :type=>"point", :geographic=>true}
  end

  add_index "lessons", ["category_id"], name: "index_lessons_on_category_id", using: :btree
  add_index "lessons", ["latlon"], name: "index_lessons_on_latlon", using: :gist
  add_index "lessons", ["sub_category_id"], name: "index_lessons_on_sub_category_id", using: :btree
  add_index "lessons", ["sub_sub_category_id"], name: "index_lessons_on_sub_sub_category_id", using: :btree
  add_index "lessons", ["user_id"], name: "index_lessons_on_user_id", using: :btree

  create_table "messages", force: :cascade do |t|
    t.string   "content"
    t.string   "chat_id"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.boolean  "read"
    t.string   "sender_id"
    t.string   "recipient_id"
    t.string   "attachment_file_name"
    t.string   "attachment_content_type"
    t.integer  "attachment_file_size"
    t.datetime "attachment_updated_at"
  end

  create_table "objectives", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
    t.string   "name"
    t.string   "code"
    t.boolean  "is_active"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "organizations", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
    t.string   "name"
    t.string   "address"
    t.uuid     "industry_id"
    t.string   "registration_number"
    t.string   "website",             default: "NA", null: false
    t.string   "contact_number"
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
    t.string   "type_of_company",     default: " ",  null: false
    t.string   "zipcode",             default: " ",  null: false
    t.text     "about",               default: " ",  null: false
  end

  add_index "organizations", ["industry_id"], name: "index_organizations_on_industry_id", using: :btree

  create_table "pay_mes", force: :cascade do |t|
    t.string   "method"
    t.string   "detail"
    t.string   "user"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "profiles", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
    t.uuid     "user_id"
    t.uuid     "user_type_id"
    t.uuid     "app_country_id"
    t.uuid     "profilable_id"
    t.string   "profilable_type"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
  end

  add_index "profiles", ["app_country_id"], name: "index_profiles_on_app_country_id", using: :btree
  add_index "profiles", ["profilable_id", "profilable_type"], name: "index_profiles_on_profilable_id_and_profilable_type", unique: true, using: :btree
  add_index "profiles", ["user_id"], name: "index_profiles_on_user_id", using: :btree
  add_index "profiles", ["user_type_id"], name: "index_profiles_on_user_type_id", using: :btree

  create_table "qualifications", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
    t.string   "name"
    t.string   "code"
    t.boolean  "is_active",  null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "reported_users", force: :cascade do |t|
    t.string   "reporter_id"
    t.string   "reportee_id"
    t.text     "content"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "reviews", force: :cascade do |t|
    t.string   "title"
    t.integer  "rating"
    t.string   "content"
    t.string   "reviewer_id"
    t.string   "lesson_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.string   "reviewee_id"
  end

  create_table "schools_attendeds", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
    t.string   "name"
    t.date     "from_date"
    t.date     "to_date"
    t.uuid     "qualification_id"
    t.boolean  "has_graduated",          default: false, null: false
    t.uuid     "tutor_qualification_id"
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.uuid     "lesson_id"
    t.integer  "start_year"
    t.integer  "end_year"
    t.string   "disciplines"
  end

  add_index "schools_attendeds", ["lesson_id"], name: "index_schools_attendeds_on_lesson_id", using: :btree
  add_index "schools_attendeds", ["qualification_id"], name: "index_schools_attendeds_on_qualification_id", using: :btree
  add_index "schools_attendeds", ["tutor_qualification_id"], name: "index_schools_attendeds_on_tutor_qualification_id", using: :btree

  create_table "shortlisted_lessons", force: :cascade do |t|
    t.string   "lesson_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "user_id"
  end

  create_table "sub_categories", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
    t.string   "name"
    t.string   "code"
    t.boolean  "is_active",   default: true, null: false
    t.uuid     "category_id"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  add_index "sub_categories", ["category_id"], name: "index_sub_categories_on_category_id", using: :btree

  create_table "sub_sub_categories", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
    t.string   "name"
    t.string   "code"
    t.boolean  "is_active",       default: true, null: false
    t.uuid     "sub_category_id"
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
  end

  add_index "sub_sub_categories", ["sub_category_id"], name: "index_sub_sub_categories_on_sub_category_id", using: :btree

  create_table "tutor_qualifications", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
    t.integer  "years_of_experience"
    t.uuid     "lesson_id"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
  end

  add_index "tutor_qualifications", ["lesson_id"], name: "index_tutor_qualifications_on_lesson_id", using: :btree

  create_table "user_types", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
    t.string   "name"
    t.string   "code"
    t.boolean  "is_active"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
    t.string    "email",                                                                           default: "",    null: false
    t.string    "encrypted_password",                                                              default: "",    null: false
    t.string    "reset_password_token"
    t.datetime  "reset_password_sent_at"
    t.datetime  "remember_created_at"
    t.integer   "sign_in_count",                                                                   default: 0,     null: false
    t.datetime  "current_sign_in_at"
    t.datetime  "last_sign_in_at"
    t.inet      "current_sign_in_ip"
    t.inet      "last_sign_in_ip"
    t.string    "confirmation_token"
    t.datetime  "confirmed_at"
    t.datetime  "confirmation_sent_at"
    t.string    "unconfirmed_email"
    t.integer   "failed_attempts",                                                                 default: 0,     null: false
    t.string    "unlock_token"
    t.datetime  "locked_at"
    t.datetime  "created_at",                                                                                      null: false
    t.datetime  "updated_at",                                                                                      null: false
    t.boolean   "is_admin",                                                                        default: false, null: false
    t.geography "latlon",                 limit: {:srid=>4326, :type=>"point", :geographic=>true}
  end

  add_index "users", ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true, using: :btree
  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["latlon"], name: "index_users_on_latlon", using: :gist
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  add_index "users", ["unlock_token"], name: "index_users_on_unlock_token", unique: true, using: :btree

end
