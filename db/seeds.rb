user_types = ["Individual", "Organization"]
user_types.each do |ut|
	UserType.find_or_create_by(code: ut.upcase.gsub(" ", "-")) do |user_type|
		user_type.name = ut
		user_type.code = ut.upcase.gsub(" ", "-")
		user_type.is_active = true
	end
end


objectives = ["Learn", "Teach", "Learn & Teach"]
objectives.each do |objective|
	Objective.find_or_create_by(code: objective.upcase.gsub(" ", "-")) do |o|
		o.name = objective
		o.code = objective.upcase.gsub(" ", "-")
		o.is_active = true
	end
end

countries = ISO3166::Country.all
countries.map { |country|
	AppCountry.find_or_create_by(id_code: country.alpha2) do |c|
		c.name = country.name
		c.id_code = country.alpha2
		c.country_code = country.country_code
		c.currency_name = country.currency.try(:name)
		c.currency_code = country.currency.try(:code)
		c.currency_symbol = country.currency.try(:symbol)
		c.latlon = "POINT(#{country.longitude_dec} #{country.latitude_dec})"
	end
}

industries = ["Accounting", "Advertising/Public Relations", "Agribusiness", "Agriculture",
	"Airlines/Aviation", "Alternative Dispute Resolution", "Alternative Medicine", "Animation",
	"Apparel & Fashion", "Architecture & Planning", "Arts & Crafts", "Automotive",
	"Aviation & Aerospace", "Banking", "Biotechnology", "Broadcast Media", "Building Materials",
	"Business Supplies and Equipment", "Capital Markets", "Chemicals", "Civic & Social Organization",
	"Civil Engineering", "Commercial Real Estate", "Computer & Network Security", "Computer Games",
	"Computer Hardware", "Computer Networking", "Computer Software", "Construction", "Consumer Electronics",
	"Consumer Goods", "Consumer Services", "Cosmetics", "Dairy", "Defense & Space", "Design",
	"Education Management", "E-learning", "Electrical/Electronic Manufacturing", "Entertainment",
	"Environmental Services", "Events Services", "Executive Office", "Facilities Services", "Farming",
	"Financial Services", "Fine Art", "Fishery", "Food & Beverages", "Food Production", "Fund-Raising",
	"Furniture", "Gambling & Casinos", "Glass, Ceramics & Concrete", "Government Administration",
	"Government Relations", "Graphic Design", "Health, Wellness & Fitness", "Higher Education",
	"Hospital & Health Care", "Hospitality", "Human Resource", "Import & Export", "Individual & Family Services",
	"Industrial Automation", "Information Services", "Information Technology & Services", "Insurance",
	"International Affairs", "International Trade & Development", "Internet", "Investment Banking",
	"Judiciary", "Law Enforcement", "Law Practice", "Legal Services", "Legislative Office", "Leisure, Travel & Tourism",
	"Libraries", "Logistics & Supply Chain", "Luxury Goods & Jewellery", "Machinery", "Management Consulting",
	"Maritime", "Market Research", "Marketing & Advertising", "Mechanical or Industrial Engineering",
	"Media Production", "Medical Devices", "Medical Practice", "Mental Health Care", "Military",
	"Mining & Metals", "Motion Pictures & Film", "Museums & Institutions", "Music", "Nanotechnology",
	"Newspapers", "Non-Profit Organization Management", "Oil & Energy", "Online Media", "Outsourcing/Offshoring",
	"Package/Freight Delivery", "Packaging and Containers", "Paper & Forest Products", "Performing Arts",
	"Pharmaceuticals", "Philanthropy", "Photography", "Plastics", "Political Organization",
	"Primary/Secondary Education", "Printing", "Professional Training", "Program Development", "Public Policy",
	"Public Relations & Commnunications", "Public Safety", "Publishing", "Railroad Manufacture", "Ranching",
	"Real Estate", "Recreational Facilities", "Religious Institutions", "Renewables & Environment",
	"Research", "Restaurants", "Retail", "Security & Investigations", "Semiconductors", "Shipbuilding",
	"Sporting Goods", "Sports", "Staffing & Recruiting", "Supermarkets", "Telecommunications",
	"Textiles", "Think Tanks", "Tobacco", "Translation & Localization", "Transportation/Trucking/Railroad",
	"Utilities", "Venture Capital & Private Equity", "Veterinary", "Warehousing", "Wholesale",
	"Wines & Spirits", "Wireless", "Writing & Editing"]
industries.each do |industry|
	Industry.find_or_create_by(code: industry.upcase.gsub(" ", "-")) do |i|
		i.name = industry
		i.code = industry.upcase.gsub(" ", "-")
		i.is_active = true
	end
end

genders = ["Male", "Female"]
genders.each do |gender|
	code = gender.upcase.gsub(" ", "-")
	Gender.find_or_create_by(code: code) do |g|
		g.name = gender
		g.code = code
		g.is_active = true
	end
end

User.find_or_create_by(email: 'admin@prevalearn.com') do |u|
	u.skip_confirmation!
	u.email = 'admin@prevalearn.com'
	u.password = "12345678"
	u.password_confirmation = "12345678"
	u.is_admin = true
end

categories = [
	"Academics", "Art", "Baking", "Career Boosters", "Certifications", "College Prep",
	"Cooking", "Cool Skills", "Craft", "Dance", "Dancing", "Driving", "Fine Art Appreciation",
	"For Kids", "Games", "Health & Wellness", "Home DIY", "Languages", "Music", "Performing Arts",
	"Pets Welfare", "Self Improvement", "Sports", "Tech", "Visual Arts"
]

categories.each do |category|
	code = category.upcase.gsub(" ", "-")
	Category.find_or_create_by(code: code) do |c|
		c.name = category
		c.code = code
		c.is_active = true
	end
end
