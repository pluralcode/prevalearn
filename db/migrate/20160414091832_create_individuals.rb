class CreateIndividuals < ActiveRecord::Migration
  def change
    create_table :individuals, id: :uuid do |t|
      t.string :postal_code
      t.string :first_name
      t.string :last_name
      t.uuid :gender_id
      t.uuid :objective_id
      t.string :contact_number

      t.timestamps null: false
    end

    add_index :individuals, :gender_id
    add_index :individuals, :objective_id
  end
end
