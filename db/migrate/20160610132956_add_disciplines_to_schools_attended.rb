class AddDisciplinesToSchoolsAttended < ActiveRecord::Migration
  def change
    add_column :schools_attendeds, :disciplines, :string
  end
end
