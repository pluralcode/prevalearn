class CreateSubSubCategories < ActiveRecord::Migration
  def change
    create_table :sub_sub_categories, id: :uuid do |t|
      t.string :name
      t.string :code
      t.boolean :is_active, null: false, default: true
      t.uuid :sub_category_id

      t.timestamps null: false
    end
    add_index :sub_sub_categories, :sub_category_id
  end
end
