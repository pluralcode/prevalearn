class AddAttachmentPhotoToLessonGalleries < ActiveRecord::Migration
  def self.up
    change_table :lesson_galleries do |t|
      t.attachment :photo
    end
  end

  def self.down
    remove_attachment :lesson_galleries, :photo
  end
end
