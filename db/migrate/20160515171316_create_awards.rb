class CreateAwards < ActiveRecord::Migration
  def change
    create_table :awards, id: :uuid do |t|
      t.string :name
      t.integer :year
      t.uuid :lesson_id

      t.timestamps null: false
    end
    add_index :awards, :lesson_id
  end
end
