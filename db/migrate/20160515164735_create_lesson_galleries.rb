class CreateLessonGalleries < ActiveRecord::Migration
  def change
    create_table :lesson_galleries, id: :uuid do |t|
      t.uuid :lesson

      t.timestamps null: false
    end
    add_index :lesson_galleries, :lesson
  end
end
