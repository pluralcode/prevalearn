class AddFieldsToMessages < ActiveRecord::Migration
  def change
  	add_column :messages, :sender_id, :string
  	add_column :messages, :recipient_id, :string

  end
end
