class AddFieldsToChats < ActiveRecord::Migration
  def change
    add_column :chats, :starred_by_sender, :boolean, default: false
    add_column :chats, :starred_by_recipient, :boolean, default: false
    add_column :chats, :deleted_by_sender, :boolean, default: false
    add_column :chats, :deleted_by_recipient, :boolean, default: false
  end
end
