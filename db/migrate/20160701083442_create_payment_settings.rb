class CreatePaymentSettings < ActiveRecord::Migration
  def change
    create_table :payment_settings do |t|
      t.string :method
      t.string :detail
      t.string :user

      t.timestamps null: false
    end
  end
end
