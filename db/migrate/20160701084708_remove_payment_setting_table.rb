class RemovePaymentSettingTable < ActiveRecord::Migration
  def change
    drop_table :payment_settings
  end
end
