class CreateLessonSpecifications < ActiveRecord::Migration
  def change
    create_table :lesson_specifications, id: :uuid do |t|
      t.uuid :class_level_id
      t.uuid :age_requirement_id
      t.integer :average_class_size
      t.integer :number_of_lessons
      t.text :fee_includes
      t.text :class_location
      t.uuid :lesson_id

      t.timestamps null: false
    end
    add_index :lesson_specifications, :class_level_id
    add_index :lesson_specifications, :age_requirement_id
    add_index :lesson_specifications, :lesson_id
  end
end
