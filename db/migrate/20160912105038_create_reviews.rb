class CreateReviews < ActiveRecord::Migration
  def change
    create_table :reviews do |t|
      t.string :title
      t.integer :rating
      t.string :content
      t.string :reviewer_id
      t.string :lesson_id

      t.timestamps null: false
    end
  end
end
