class AddArchivedToChats < ActiveRecord::Migration
  def change
    add_column :chats, :archived, :boolean, default: false
  end
end
