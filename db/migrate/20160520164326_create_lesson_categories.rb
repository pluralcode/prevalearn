class CreateLessonCategories < ActiveRecord::Migration
  def change
    create_table :lesson_categories do |t|
      t.uuid :lesson_id
      t.uuid :category_id
      t.uuid :sub_category_id
      t.uuid :sub_sub_category_id

      t.timestamps null: false
    end
    add_index :lesson_categories, :lesson_id
    add_index :lesson_categories, :category_id
    add_index :lesson_categories, :sub_category_id
    add_index :lesson_categories, :sub_sub_category_id
  end
end
