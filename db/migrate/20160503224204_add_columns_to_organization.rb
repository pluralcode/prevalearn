class AddColumnsToOrganization < ActiveRecord::Migration
  def change
    add_column :organizations, :type_of_company, :string, null: false, default: " "
    add_column :organizations, :zipcode, :string, null: false, default: " "
    add_column :organizations, :about, :text, null: false, default: " "
  end
end
