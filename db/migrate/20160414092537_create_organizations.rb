class CreateOrganizations < ActiveRecord::Migration
  def change
    create_table :organizations, id: :uuid do |t|
      t.string :name
      t.string :address
      t.uuid :industry_id
      t.string :registration_number
      t.string :website
      t.string :contact_number

      t.timestamps null: false
    end

    add_index :organizations, :industry_id
  end
end
