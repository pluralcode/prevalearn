class AddLessonAmountToBookings < ActiveRecord::Migration
  def change
    add_column :bookings, :lesson_amount, :float
  end
end
