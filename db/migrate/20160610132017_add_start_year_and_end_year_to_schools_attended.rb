class AddStartYearAndEndYearToSchoolsAttended < ActiveRecord::Migration
  def change
    add_column :schools_attendeds, :start_year, :integer
    add_column :schools_attendeds, :end_year, :integer
  end
end
