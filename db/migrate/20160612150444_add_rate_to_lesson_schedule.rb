class AddRateToLessonSchedule < ActiveRecord::Migration
  def change
    add_column :lesson_schedules, :rate, :decimal, null: false, default: 0.0
  end
end
