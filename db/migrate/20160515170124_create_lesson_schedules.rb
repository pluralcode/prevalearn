class CreateLessonSchedules < ActiveRecord::Migration
  def change
    create_table :lesson_schedules, id: :uuid do |t|
      t.date :lesson_date
      t.time :from_time
      t.time :to_time
      t.uuid :lesson_id

      t.timestamps null: false
    end
    add_index :lesson_schedules, :lesson_id
  end
end
