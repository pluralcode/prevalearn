class AddUserIdToShortlistedLessons < ActiveRecord::Migration
  def change
    add_column :shortlisted_lessons, :user_id, :string
  end
end
