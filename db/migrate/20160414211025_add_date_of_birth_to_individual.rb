class AddDateOfBirthToIndividual < ActiveRecord::Migration
  def change
    add_column :individuals, :date_of_birth, :date
  end
end
