class AddDefaultValueToWebsite < ActiveRecord::Migration
	def change
		change_column :organizations, :website, :string, null: false, default: "NA"
	end
end
