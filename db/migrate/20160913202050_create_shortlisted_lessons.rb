class CreateShortlistedLessons < ActiveRecord::Migration
  def change
    create_table :shortlisted_lessons do |t|
      t.string :lesson_id

      t.timestamps null: false
    end
  end
end
