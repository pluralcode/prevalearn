class CreateReportedUsers < ActiveRecord::Migration
  def change
    create_table :reported_users do |t|
      t.string :reporter_id
      t.string :reportee_id
      t.text :content

      t.timestamps null: false
    end
  end
end
