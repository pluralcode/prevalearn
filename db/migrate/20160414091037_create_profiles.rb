class CreateProfiles < ActiveRecord::Migration
  def change
    create_table :profiles, id: :uuid do |t|
      t.uuid :user_id
      t.uuid :user_type_id
      t.uuid :app_country_id
      t.uuid :profilable_id
      t.string :profilable_type

      t.timestamps null: false
    end

    add_index :profiles, :user_id
    add_index :profiles, :user_type_id
    add_index :profiles, :app_country_id
    add_index :profiles, [:profilable_id, :profilable_type], unique: true
  end
end
