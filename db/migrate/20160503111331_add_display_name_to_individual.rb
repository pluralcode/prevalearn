class AddDisplayNameToIndividual < ActiveRecord::Migration
  def change
    add_column :individuals, :display_name, :string, null: false, default: " "
  end
end
