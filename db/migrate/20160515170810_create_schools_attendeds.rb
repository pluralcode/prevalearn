class CreateSchoolsAttendeds < ActiveRecord::Migration
  def change
    create_table :schools_attendeds, id: :uuid do |t|
      t.string :name
      t.date :from_date
      t.date :to_date
      t.uuid :qualification_id
      t.boolean :has_graduated, null: false, default: false
      t.uuid :tutor_qualification_id

      t.timestamps null: false
    end
    add_index :schools_attendeds, :qualification_id
    add_index :schools_attendeds, :tutor_qualification_id
  end
end
