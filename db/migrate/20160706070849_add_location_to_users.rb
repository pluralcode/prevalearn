class AddLocationToUsers < ActiveRecord::Migration
  def change
  	enable_extension "postgis"
    add_column :users, :latlon, :st_point, :geographic => true
    add_index :users, :latlon, using: :gist
  end
end
