class AddLessonIdToChats < ActiveRecord::Migration
  def change
    add_column :chats, :lesson_id, :string
  end
end
