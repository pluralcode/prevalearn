class AddLessonIdToTutorQualification < ActiveRecord::Migration
  def change
    add_column :schools_attendeds, :lesson_id, :uuid
    add_index :schools_attendeds, :lesson_id
  end
end
