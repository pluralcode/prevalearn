class CreateAppCountries < ActiveRecord::Migration
  def change
    create_table :app_countries, id: :uuid do |t|
      t.string :name
      t.string :id_code
      t.string :country_code
      t.string :currency_name
      t.string :currency_code
      t.string :currency_symbol

      t.timestamps null: false
    end
  end
end
