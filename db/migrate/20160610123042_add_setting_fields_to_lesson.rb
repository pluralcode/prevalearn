class AddSettingFieldsToLesson < ActiveRecord::Migration
  def change
    add_column :lessons, :is_age_public, :boolean, null: false, default: false
    add_column :lessons, :is_hidden_from_listing, :boolean, null: false, default: false
  end
end
