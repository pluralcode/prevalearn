class CreateMessages < ActiveRecord::Migration
  def change
    create_table :messages do |t|
      t.string :content
      t.string :chat_id

      t.timestamps null: false
    end
  end
end
