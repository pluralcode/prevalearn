class CreateLessons < ActiveRecord::Migration
  def change
    create_table :lessons, id: :uuid do |t|
      t.string :title
      t.text :about_tutor
      t.uuid :category_id
      t.uuid :sub_category_id
      t.uuid :sub_sub_category_id
      t.text :class_description
      t.boolean :has_schedule, null: false, default: false
      t.uuid :user_id

      t.timestamps null: false
    end
    
    add_index :lessons, :category_id
    add_index :lessons, :sub_category_id
    add_index :lessons, :sub_sub_category_id
    add_index :lessons, :user_id
  end
end
