class RemoveNumberOfLessonsFromLessonSpecification < ActiveRecord::Migration
  def change
    remove_column :lesson_specifications, :number_of_lessons, :integer
    add_column :lesson_specifications, :number_of_lessons, :string
  end
end
