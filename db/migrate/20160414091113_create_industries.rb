class CreateIndustries < ActiveRecord::Migration
  def change
    create_table :industries, id: :uuid do |t|
      t.string :name
      t.string :code
      t.string :is_active

      t.timestamps null: false
    end
  end
end
