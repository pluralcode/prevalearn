class CreateAgeRequirements < ActiveRecord::Migration
  def change
    create_table :age_requirements, id: :uuid do |t|
      t.string :name
      t.boolean :is_active, null: false, is_active: true

      t.timestamps null: false
    end
  end
end
