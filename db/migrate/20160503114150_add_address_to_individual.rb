class AddAddressToIndividual < ActiveRecord::Migration
  def change
    add_column :individuals, :address, :text, null: false, default: " "
  end
end
