class AddLessonIdToBookings < ActiveRecord::Migration
  def change
    add_column :bookings, :lesson_id, :string
  end
end
