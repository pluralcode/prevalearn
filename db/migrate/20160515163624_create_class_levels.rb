class CreateClassLevels < ActiveRecord::Migration
  def change
    create_table :class_levels, id: :uuid do |t|
      t.string :name
      t.string :code
      t.boolean :is_active, null: false, is_active: true

      t.timestamps null: false
    end
  end
end
