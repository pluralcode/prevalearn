class AddRevieweeIdToReviews < ActiveRecord::Migration
  def change
    add_column :reviews, :reviewee_id, :string
  end
end
