class CreateUserTypes < ActiveRecord::Migration
  def change
    create_table :user_types, id: :uuid do |t|
      t.string :name
      t.string :code
      t.boolean :is_active

      t.timestamps null: false
    end
  end
end
