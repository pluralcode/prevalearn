class CreateCategories < ActiveRecord::Migration
  def change
    create_table :categories, id: :uuid do |t|
      t.string :name
      t.string :code
      t.boolean :is_active, null: false, default: true

      t.timestamps null: false
    end
  end
end
