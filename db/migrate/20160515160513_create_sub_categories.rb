class CreateSubCategories < ActiveRecord::Migration
  def change
    create_table :sub_categories, id: :uuid do |t|
      t.string :name
      t.string :code
      t.boolean :is_active, null: false, default: true
      t.uuid :category_id

      t.timestamps null: false
    end
    add_index :sub_categories, :category_id
  end
end
