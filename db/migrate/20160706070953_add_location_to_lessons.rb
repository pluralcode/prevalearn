class AddLocationToLessons < ActiveRecord::Migration
  def change
  	enable_extension "postgis"
    add_column :lessons, :latlon, :st_point, :geographic => true
    add_index :lessons, :latlon, using: :gist
  end
end
