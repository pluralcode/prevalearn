class CreateTutorQualifications < ActiveRecord::Migration
  def change
    create_table :tutor_qualifications, id: :uuid do |t|
      t.integer :years_of_experience
      t.uuid :lesson_id

      t.timestamps null: false
    end
    add_index :tutor_qualifications, :lesson_id
  end
end
