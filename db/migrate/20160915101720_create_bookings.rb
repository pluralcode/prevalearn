class CreateBookings < ActiveRecord::Migration
  def change
    create_table :bookings do |t|
      t.string :lesson_schedule_id
      t.string :instructor_id
      t.string :student_id
      t.string :payment_status

      t.timestamps null: false
    end
  end
end
