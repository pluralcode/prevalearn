class CreatePayMes < ActiveRecord::Migration
  def change
    create_table :pay_mes do |t|
      t.string :method
      t.string :detail
      t.string :user

      t.timestamps null: false
    end
  end
end
