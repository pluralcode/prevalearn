class AddLocationToCountries < ActiveRecord::Migration
  def change
  	enable_extension "postgis"

    add_column :app_countries, :latlon, :st_point, :geographic => true
    add_index :app_countries, :latlon, using: :gist
  end
end
