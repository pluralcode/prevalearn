class CreateDisciplines < ActiveRecord::Migration
  def change
    create_table :disciplines, id: :uuid do |t|
      t.string :name
      t.uuid :school_attended_id

      t.timestamps null: false
    end
    add_index :disciplines, :school_attended_id
  end
end
