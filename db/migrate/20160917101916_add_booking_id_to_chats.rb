class AddBookingIdToChats < ActiveRecord::Migration
  def change
    add_column :chats, :booking_id, :string
  end
end
