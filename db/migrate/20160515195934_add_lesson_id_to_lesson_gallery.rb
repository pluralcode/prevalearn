class AddLessonIdToLessonGallery < ActiveRecord::Migration
  def change
    add_column :lesson_galleries, :lesson_id, :uuid
    add_index :lesson_galleries, :lesson_id

    remove_index :lesson_galleries, :lesson
    remove_column :lesson_galleries, :lesson, :uuid
  end
end
